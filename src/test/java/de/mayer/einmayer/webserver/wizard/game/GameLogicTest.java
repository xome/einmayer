package de.mayer.einmayer.webserver.wizard.game;

import de.mayer.einmayer.webserver.dao.wizard.*;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameJoinOrLeaveMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameMessageType;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.GameClosedMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.LobbyMessageType;
import org.assertj.core.groups.Tuple;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
public class GameLogicTest {

    @Test
    public void whenLastParticipantLeaves_ThenAllParticipantsGetSetToLeft() {

        WizardGameParticipantRepo mockedWizardParticipantRepo = Mockito.mock(WizardGameParticipantRepo.class);
        var gameId = 1;
        var user = "test";
        var id = new WizardGameParticipantId();
        id.setNameUser(user);
        id.setIdGame(gameId);
        var participant = new WizardGameParticipant();
        participant.setSeat(1);
        participant.setNameUser(user);
        participant.setIdGame(gameId);
        participant.setStat(WizardGameParticipantStat.IN_GAME.toString());
        Optional<WizardGameParticipant> optional = Optional.of(participant);
        Mockito
                .when(mockedWizardParticipantRepo.findById(id))
                .thenReturn(optional);
        Mockito
                .when(mockedWizardParticipantRepo.countByIdGameAndStat(gameId, WizardGameParticipantStat.IN_GAME.toString()))
                .thenReturn(1L);

        Mockito
                .when(mockedWizardParticipantRepo.findAllByIdGame(gameId))
                .thenReturn(Collections.singletonList(participant));

        var soll = new WizardGameParticipant();
        soll.setIdGame(gameId);
        soll.setNameUser(user);
        soll.setSeat(1);
        soll.setStat(WizardGameParticipantStat.LEFT.toString());

        List<WizardGameParticipant> istListe = new ArrayList<>();

        Mockito
                .when(mockedWizardParticipantRepo.saveAll(Mockito.anyIterable()))
                .then(invocationOnMock -> {
                    Object[] args = invocationOnMock.getArguments();
                    istListe.addAll((Collection<? extends WizardGameParticipant>) args[0]);
                    return istListe;
                });

        WizardGameRepo mockedGameRepo = Mockito.mock(WizardGameRepo.class);

        WizardGame game = new WizardGame();
        game.setId(gameId);
        game.setRound(0);
        game.setStat(WizardGameStat.OPEN.toString());

        Mockito
                .when(mockedGameRepo.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(game));

        SimpMessageSendingOperations messageSender =
                Mockito.mock(SimpMessageSendingOperations.class);

        GameLogic logic = new GameLogic();
        logic.setWizardGameParticipantRepo(mockedWizardParticipantRepo);
        logic.setWizardGameRepo(mockedGameRepo);
        logic.setMessageOperations(messageSender);

        logic.userLeft(gameId, user);

        assertThat(istListe.size(), greaterThanOrEqualTo(1));
        assertThat(istListe.get(0), is(soll));
        assertThat(istListe.get(0).getStat(), is(WizardGameParticipantStat.LEFT.toString()));
    }

    @Test
    public void whenLastParticipantLeaves_ThenGameGetsClosed() {

        WizardGameParticipantRepo mockedWizardParticipantRepo = Mockito.mock(WizardGameParticipantRepo.class);
        var gameId = 1;
        var user = "test";
        var id = new WizardGameParticipantId();
        id.setNameUser(user);
        id.setIdGame(gameId);
        var participant = new WizardGameParticipant();
        participant.setSeat(1);
        participant.setNameUser(user);
        participant.setIdGame(gameId);
        participant.setStat(WizardGameParticipantStat.IN_GAME.toString());
        Optional<WizardGameParticipant> optional = Optional.of(participant);
        Mockito
                .when(mockedWizardParticipantRepo.findById(id))
                .thenReturn(optional);
        Mockito
                .when(mockedWizardParticipantRepo.countByIdGameAndStat(gameId, WizardGameParticipantStat.IN_GAME.toString()))
                .thenReturn(1L);

        Mockito
                .when(mockedWizardParticipantRepo.findAllByIdGame(gameId))
                .thenReturn(Collections.singletonList(participant));

        var soll = new WizardGameParticipant();
        soll.setIdGame(gameId);
        soll.setNameUser(user);
        soll.setSeat(1);
        soll.setStat(WizardGameParticipantStat.LEFT.toString());

        List<WizardGameParticipant> istListe = new ArrayList<>();

        Mockito
                .when(mockedWizardParticipantRepo.saveAll(Mockito.anyIterable()))
                .then(invocationOnMock -> {
                    Object[] args = invocationOnMock.getArguments();
                    istListe.addAll((Collection<? extends WizardGameParticipant>) args[0]);
                    return istListe;
                });

        WizardGameRepo mockedGameRepo = Mockito.mock(WizardGameRepo.class);

        WizardGame game = new WizardGame();
        game.setId(gameId);
        game.setRound(0);
        game.setStat(WizardGameStat.OPEN.toString());

        Mockito
                .when(mockedGameRepo.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(game));

        AtomicReference<WizardGame> gameRef = new AtomicReference<>();

        Mockito
                .when(mockedGameRepo.save(Mockito.any(WizardGame.class)))
                .then(invocationOnMock -> {
                    gameRef.set(invocationOnMock.getArgument(0));
                    return invocationOnMock.getArgument(0);
                });

        SimpMessageSendingOperations messageSender =
                Mockito.mock(SimpMessageSendingOperations.class);

        GameLogic logic = new GameLogic();
        logic.setWizardGameParticipantRepo(mockedWizardParticipantRepo);
        logic.setWizardGameRepo(mockedGameRepo);
        logic.setMessageOperations(messageSender);

        logic.userLeft(gameId, user);

        var ist = gameRef.get();

        assertThat(ist, is(notNullValue()));
        assertThat(ist.getStat(), is(WizardGameStat.CLOSED.toString()));

    }

    @Test
    public void whenParticipantLeaves_ThenGameChannelAndLobbyChannelGetsMessage() {

        WizardGameParticipantRepo mockedWizardParticipantRepo = Mockito.mock(WizardGameParticipantRepo.class);
        var gameId = 1;
        var user = "test";
        var id = new WizardGameParticipantId();
        id.setNameUser(user);
        id.setIdGame(gameId);
        var participant = new WizardGameParticipant();
        participant.setSeat(1);
        participant.setNameUser(user);
        participant.setIdGame(gameId);
        participant.setStat(WizardGameParticipantStat.IN_GAME.toString());
        Optional<WizardGameParticipant> optional = Optional.of(participant);
        Mockito
                .when(mockedWizardParticipantRepo.findById(id))
                .thenReturn(optional);
        Mockito
                .when(mockedWizardParticipantRepo.countByIdGameAndStat(gameId, WizardGameParticipantStat.IN_GAME.toString()))
                .thenReturn(1L);

        Mockito
                .when(mockedWizardParticipantRepo.findAllByIdGame(gameId))
                .thenReturn(Collections.singletonList(participant));

        WizardGameRepo mockedGameRepo = Mockito.mock(WizardGameRepo.class);

        WizardGame game = new WizardGame();
        game.setId(gameId);
        game.setRound(0);
        game.setStat(WizardGameStat.OPEN.toString());

        Mockito
                .when(mockedGameRepo.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(game));

        SimpMessageSendingOperations messageSender =
                Mockito.mock(SimpMessageSendingOperations.class);

        HashMap<String, GameJoinOrLeaveMessage> gameLeaveMessages = new HashMap<>();
        HashMap<String, GameClosedMessage> gameClosedMessage = new HashMap<>();

        Mockito
                .doAnswer(invocationOnMock -> {
                    gameLeaveMessages.put(invocationOnMock.getArgument(0),
                            invocationOnMock.getArgument(1));
                    return null;})
                .when(messageSender)
                .convertAndSend(Mockito.any(String.class), Mockito.any(GameJoinOrLeaveMessage.class));

        Mockito
                .doAnswer(invocationOnMock -> {
                    gameClosedMessage.put(invocationOnMock.getArgument(0),
                            invocationOnMock.getArgument(1));
                    return null;})
                .when(messageSender)
                .convertAndSend(Mockito.any(String.class), Mockito.any(GameClosedMessage.class));

        GameLogic logic = new GameLogic();
        logic.setWizardGameParticipantRepo(mockedWizardParticipantRepo);
        logic.setWizardGameRepo(mockedGameRepo);
        logic.setMessageOperations(messageSender);

        logic.userLeft(gameId, user);

        assertThat(gameLeaveMessages.size(), is(2));

        gameLeaveMessages.forEach((channel, msg) -> {
            assertThat(channel, is(notNullValue()));
            assertThat(msg, is(notNullValue()));
            assertThat(channel, anyOf(is("/lobby/games/1"), is("/lobby/public")));
            assertThat(msg.getSender(), is(user));
            assertThat(msg.getSeat(), is(1));
            assertThat(msg.getMsgType(), is(GameMessageType.LEAVE));
            assertThat(msg.getGameId(), is(gameId));
        });

        assertThat(gameClosedMessage.size(), is(1));
        assertThat(gameClosedMessage, hasKey("/lobby/public"));
        var closedMsg = gameClosedMessage.get("/lobby/public");

        assertThat(closedMsg.getGame().getId(), is(gameId));
        assertThat(closedMsg.getGame().getStat(), is(WizardGameStat.CLOSED.toString()));
        assertThat(closedMsg.getSender(), is(GameClosedMessage.SENDER_SERVER));
        assertThat(closedMsg.getMsgType(), is(LobbyMessageType.CLOSED_GAME));
    }

    @Test
    public void whenThereAreParticipantsLeft_ThenGameStaysOpenAndMessageIsSentToGameChannelAndLobbyChannel() {

        WizardGameParticipantRepo mockedWizardParticipantRepo = Mockito.mock(WizardGameParticipantRepo.class);
        var gameId = 1;
        var user = "test";
        var id = new WizardGameParticipantId();
        id.setNameUser(user);
        id.setIdGame(gameId);
        var participantLeaving = new WizardGameParticipant();
        participantLeaving.setSeat(1);
        participantLeaving.setNameUser(user);
        participantLeaving.setIdGame(gameId);
        participantLeaving.setStat(WizardGameParticipantStat.IN_GAME.toString());
        var participantStaying = new WizardGameParticipant();
        participantStaying.setIdGame(gameId);
        participantStaying.setNameUser("staying");
        participantStaying.setSeat(2);
        participantStaying.setStat(WizardGameParticipantStat.IN_GAME.toString());

        Optional<WizardGameParticipant> optional = Optional.of(participantLeaving);
        Mockito
                .when(mockedWizardParticipantRepo.findById(id))
                .thenReturn(optional);
        Mockito
                .when(mockedWizardParticipantRepo.countByIdGameAndStat(gameId, WizardGameParticipantStat.IN_GAME.toString()))
                .thenReturn(2L);

        Mockito
                .when(mockedWizardParticipantRepo.findAllByIdGame(gameId))
                .thenReturn(Arrays.asList(participantLeaving, participantStaying));


        Mockito
                .when(mockedWizardParticipantRepo.saveAll(Mockito.anyIterable()))
                .then(invocationOnMock -> invocationOnMock.getArgument(0));

        WizardGameRepo mockedGameRepo = Mockito.mock(WizardGameRepo.class);

        WizardGame game = new WizardGame();
        game.setId(gameId);
        game.setRound(0);
        game.setStat(WizardGameStat.OPEN.toString());

        Mockito
                .when(mockedGameRepo.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(game));

        AtomicReference<WizardGame> gameRef = new AtomicReference<>();

        Mockito
                .when(mockedGameRepo.save(Mockito.any(WizardGame.class)))
                .then(invocationOnMock -> {
                    gameRef.set(invocationOnMock.getArgument(0, WizardGame.class));
                    return gameRef.get();
                });

        ArgumentCaptor<String> channelArg = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<GameJoinOrLeaveMessage> msgArg = ArgumentCaptor.forClass(GameJoinOrLeaveMessage.class);
        SimpMessageSendingOperations messageSender = Mockito.mock(SimpMessageSendingOperations.class);

        HashMap<String, GameJoinOrLeaveMessage> messages = new HashMap<>();

        Mockito
                .doAnswer(invocationOnMock -> {
                    messages.put(invocationOnMock.getArgument(0),
                            invocationOnMock.getArgument(1));
                    return null;})
                .when(messageSender)
                .convertAndSend(Mockito.any(String.class), Mockito.any(GameJoinOrLeaveMessage.class));

        GameLogic logic = new GameLogic();
        logic.setWizardGameParticipantRepo(mockedWizardParticipantRepo);
        logic.setWizardGameRepo(mockedGameRepo);
        logic.setMessageOperations(messageSender);

        logic.userLeft(gameId, user);

        var gameDurchRepositoryGeaendert = gameRef.get();
        assertThat(gameDurchRepositoryGeaendert, is(nullValue()));

        assertThat(messages.size(), is(2));

        messages.forEach((channel, msg) -> {
            assertThat(channel, is(notNullValue()));
            assertThat(msg, is(notNullValue()));
            assertThat(channel, anyOf(is("/lobby/games/1"), is("/lobby/public")));
            assertThat(msg.getSender(), is(user));
            assertThat(msg.getSeat(), is(1));
            assertThat(msg.getMsgType(), is(GameMessageType.LEAVE));
            assertThat(msg.getGameId(), is(gameId));
        });

    }


}
package de.mayer.einmayer.webserver;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.persistence.EntityManager;
import java.io.*;

@Service
public class DatabaseService {

    @Autowired
    EntityManager em;

    public void initDatabase() throws FileNotFoundException {
        File initScript = ResourceUtils.getFile("classpath:database_init");
        StringBuilder sqlBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(initScript))){
            String line = null;
            while ((line = br.readLine()) != null){
                sqlBuilder.append(line).append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        em.createNativeQuery(sqlBuilder.toString()).executeUpdate();

    }

}

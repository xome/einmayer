package de.mayer.einmayer.webserver;

import de.mayer.einmayer.webserver.dao.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assume.assumeTrue;

import java.sql.Timestamp;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepoTests {


    @Test
    public void einkaufsListeLaesstSichAnlegen(){
        Einkaufsliste einkaufsliste = new Einkaufsliste();
        String uuid = UUID.randomUUID().toString();
        einkaufsliste.setName(uuid);
        einkaufsliste.setStatus("10");
        einkaufsliste.setTimeAen(new Timestamp(System.currentTimeMillis()));
        einkaufsliste.setTimeNeu(new Timestamp(System.currentTimeMillis()));

        einkaufslisteRepo.save(einkaufsliste);

        assertThat(einkaufslisteRepo.findByName(uuid).orElseThrow(()->new RuntimeException("Wurde nicht angelegt")).getName(),
                is(uuid));

        einkaufslisteRepo.deleteByName(uuid);
    }
    @Test
    public void einkaufsListenItemsLassenSichAnlegen(){
        Einkaufsliste einkaufsliste = new Einkaufsliste();
        String uuid = UUID.randomUUID().toString();
        einkaufsliste.setName(uuid);
        einkaufsliste.setStatus("10");
        einkaufsliste.setTimeAen(new Timestamp(System.currentTimeMillis()));
        einkaufsliste.setTimeNeu(new Timestamp(System.currentTimeMillis()));

        einkaufsliste = einkaufslisteRepo.save(einkaufsliste);

        EinkaufslisteItems item = new EinkaufslisteItems();
        item.setEinkaufslisteId(einkaufsliste.getId());
        item.setStatus("10");
        item.setTimeNeu(new Timestamp(System.currentTimeMillis()));
        item.setTimeAen(new Timestamp(System.currentTimeMillis()));
        item.setItem("Apfel");
        einkaufslisteItemRepo.save(item);

        assumeTrue(einkaufslisteItemRepo.existsById(new EinkaufslisteItemId(einkaufsliste.getId(), item.getItem())));

        einkaufslisteRepo.deleteByName(uuid);
    }



    private EinkaufslisteItemRepo einkaufslisteItemRepo;

    @Autowired
    public void setEinkaufslisteItemRepo(EinkaufslisteItemRepo einkaufslisteItemRepo) {
        this.einkaufslisteItemRepo = einkaufslisteItemRepo;
    }

    private EinkaufslisteRepo einkaufslisteRepo;

    @Autowired
    public void setEinkaufslisteRepo(EinkaufslisteRepo einkaufslisteRepo) {
        this.einkaufslisteRepo = einkaufslisteRepo;
    }
}

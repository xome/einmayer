eggiApp.controller('EinkaufslisteOverviewController', function ($scope, $http, $location, $mdToast, $mdDialog, $rootScope) {


    $scope.deletedLists = [];
    $scope.deleteIndex = -1;
    $scope.loading = true;

    $rootScope.getOverviewTx()
        .getAll().onsuccess = function (event) {
        $scope.listen = event.target.result;
        if ($scope.listen.length > 0) {
            $scope.loading = false;
        }
        $scope.listen.forEach(function (liste) {
            liste.timeAen = new Date(liste.timeAen);
            if (liste.id < 0) {
                // gibt es schon eine im Scope?
                var listeUeberspringen = false;
                $scope.listen.forEach(function (suchListe) {
                    if (suchListe.name === liste.name && suchListe.id !== liste.id) {
                        if (suchListe.id <= 0) {
                            $scope.listen.splice($scope.listen.indexOf(suchListe), 1);
                        } else {
                            $rootScope.getOverviewTx().delete(liste.id);
                            $rootScope.listen.splice($scope.listen.indexOf(liste), 1);
                            listeUeberspringen = true;
                        }
                    }
                });
                if (!listeUeberspringen) {
                    // noch nicht an Server geschickt.
                    $http.post('/api/einkaufsliste/add_liste', JSON.stringify(liste)).then(
                        function success(resp) {
                            $http.post('/api/einkaufsliste/get_liste_by_name', liste.name).then(
                                function success(resp) {
                                    $scope.listen.splice($scope.listen.indexOf(liste), 1, resp.data);
                                    $rootScope.getOverviewTx()
                                        .add(resp.data);
                                    $rootScope.getItemStore()
                                        .index('einkaufsliste')
                                        .openCursor(IDBKeyRange.only([liste.id]))
                                        .onsuccess = function (e) {
                                        var items = [];
                                        var cursor = e.target.result;
                                        if (cursor) {
                                            var itemOffline = cursor.value;
                                            itemOffline.timeAen = new Date(itemOffline.timeAen);
                                            items.push(itemOffline);
                                            cursor.continue();
                                        } else {
                                            items.forEach(function (item) {
                                                var itemNeueListe = item;
                                                itemNeueListe.einkaufslisteId = resp.data.id;
                                                $rootScope.getItemStore()
                                                    .put(itemNeueListe);
                                                $rootScope.getItemStore()
                                                    .delete([item.id, item.item])
                                            });

                                        }
                                    };
                                    $rootScope.getOverviewTx()
                                        .delete(liste.id);
                                }
                            )
                        }
                    )
                }
            }
        });
        $http.post('/api/einkaufsliste/get_all_listen')
            .then(function success(resp) {
                console.log("Listen vor HTTP: " + JSON.stringify($scope.listen));
                var listen = resp.data;
                console.log("Listen aus HTTP: " + JSON.stringify(listen));
                listen.forEach(function (liste) {
                    liste.timeAen = new Date(liste.timeAen);
                    $rootScope.getOverviewTx()
                        .openCursor(IDBKeyRange.only(liste.id))
                        .onsuccess = function (e) {
                        var result = e.target.result;
                        if (result) {
                            result.value.timeAen = new Date(result.value.timeAen);
                            if (liste.timAen < result.value.timeAen) {
                                result.update(liste);
                            } else if (liste.timeAen > result.value.timeAen) {
                                $http.post('/api/einkaufsliste/update_liste', JSON.stringify(liste));
                            }
                        } else {
                            var found = false;
                            $scope.listen.forEach(function(listeSchonInScope){
                                if (listeSchonInScope.name === liste.name){
                                    found = true;
                                    if (listeSchonInScope.id <= 0){
                                        $rootScope.getOverviewTx().delete(listeSchonInScope.id);
                                        $scope.listen.splice($scope.listen.indexOf(listeSchonInScope), 1);
                                    }
                                }
                            });
                            if (!found)
                                $rootScope.getOverviewTx().add(liste);
                        }
                    }
                });
                $scope.loading = false;
            });

    };


    $scope.jumpToListe = function (liste) {
        $location.path("/einkaufsliste/" + liste.id)
    };

    $scope.deleteListe = function (liste) {
        liste.status = '80';
        $scope.listen.splice($scope.listen.indexOf(liste), 1, liste);
        $scope.deletedLists.push(liste);
        $scope.deleteIndex++;
        $rootScope.getOverviewTx()
            .delete(liste.id)
            .onsuccess = function (e) {

            $rootScope.getOverviewTx()
                .add(liste)
                .onsuccess = function (e) {
                $http.post('/api/einkaufsliste/remove_liste', JSON.stringify(liste)).then(
                    function success(resp) {
                        $scope.showUndoToast();
                    }
                )
            }
        };
    };

    $scope.showUndoToast = function () {
        $mdToast.show({
            hideDelay: 0,
            position: 'bottom right',
            controller: 'ToastCtrl',
            controllerAs: 'ctrl',
            bindToController: true,
            locals: {},
            templateUrl: '/app/einkaufsliste/templates/undo_toast.html'
        });
    };

    $scope.$on('undoAction', function (event, data) {
        console.log(data);
        var liste = $scope.deletedLists[$scope.deleteIndex];
        var index = $scope.listen.indexOf(liste);
        console.log(index);
        liste.status = '10';
        $scope.listen.splice(index, 1, liste);
        $scope.deletedLists.splice($scope.deleteIndex, 1);
        $scope.deleteIndex--;
        $http.post('/api/einkaufsliste/add_liste', JSON.stringify({name: liste.name}));
        $rootScope.getOverviewTx().put(liste);

    });

    $scope.addListe = function (liste) {
        liste = liste.substr(0, 40);
        var listeObj = {
            id: new Date().getMilliseconds() * -1,
            name: liste,
            status: '00',
            time_neu: new Date(),
            time_aen: new Date()
        };
        $rootScope.getOverviewTx().add(listeObj);
        $scope.listen.push(listeObj);
        var idx = $scope.listen.indexOf(listeObj);
        $http.post('/api/einkaufsliste/add_liste', JSON.stringify({name: liste})).then(
            function success(resp) {
                $http.post('/api/einkaufsliste/get_liste_by_name', liste).then(
                    function success(resp) {
                        $scope.listen.splice(idx, 1, resp.data);
                        $rootScope.getOverviewTx()
                            .add(resp.data);
                        $rootScope.getOverviewTx()
                            .delete(listeObj);
                    }
                )
            }
        )
    };

    $scope.addListePopup = function (ev) {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var hour = String(today.getHours()).padStart(2, "0");
        var min = String(today.getMinutes()).padStart(2, "0");
        var sec = String(today.getSeconds()).padStart(2, "0");
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('Liste erstellen')
            .textContent('Wie soll die Liste heißen?')
            .placeholder('Liste vom ' + dd + "." + mm + "." + yyyy + " um " + hour + ":" + min + ":" + sec)
            .initialValue('Liste vom ' + dd + "." + mm + "." + yyyy + " um " + hour + ":" + min + ":" + sec)
            .targetEvent(ev)
            .required(true)
            .ok('Okay!')
            .cancel('Abbrechen');

        $mdDialog.show(confirm).then(function (result) {
            $scope.addListe(result);
        }, function () {
        });
    };


})
;
eggiApp.controller('EinkaufslisteController', function ($scope, $http, $routeParams, $mdDialog, $mdToast, $rootScope) {

    $scope.items = [];
    $scope.deletedItems = [];
    $scope.deleteIndex = -1;
    $scope.checkedItems = [];
    $scope.loading = true;
    var listeId = $routeParams.listeId;
    console.log("ID LISTE: " + listeId);
    $rootScope.getItemStore()
        .index('einkaufsliste')
        .openCursor(IDBKeyRange.only([listeId]))
        .onsuccess = function(e){
        var cursor = e.target.result;
        if(cursor){
            var itemOffline = cursor.value;
            itemOffline.timeAen = new Date(itemOffline.timeAen);
            $scope.items.push(itemOffline);
            console.log("PUSHED ITEM: " + itemOffline);
            cursor.continue();
        } else {
            if (!$scope.items) $scope.items = [];
            $http.post('/api/einkaufsliste/get_posis_by_liste', {id: listeId}).then(
                function (resp) {
                    $scope.loading = false;
                    console.log("Items vor HTTP: " + JSON.stringify($scope.items));
                    var items = resp.data;
                    console.log("Items aus HTTP: " + JSON.stringify(items));
                    items.forEach(function (itemAusHttp) {
                        var found = false;
                        itemAusHttp.timeAen = new Date(itemAusHttp.timeAen);
                        $scope.items.some(function (itemLocal) {
                            if (itemLocal.item === itemAusHttp.item) {
                                found = true;
                                itemLocal.timeAen = new Date(itemLocal.timeAen);
                                if (itemLocal.timeAen > itemAusHttp.timeAen)
                                    $http.post('/api/einkaufsliste/update_item', JSON.stringify(itemLocal));
                                else if (itemLocal.timeAen < itemAusHttp.timeAen)
                                    $rootScope.getItemStore($routeParams.listeId)
                                        .put(itemAusHttp);
                                return true;
                            }
                        });
                        if (!found) {
                            $scope.items.push(itemAusHttp);
                            $rootScope.getItemStore($routeParams.listeId).add(itemAusHttp);
                        }
                    });


                    $scope.items.forEach(function (item) {
                        item.checked = item.status === '50';
                    })
                }
            );
        }
    };


    $scope.deleteItem = function (item) {
        $scope.deletedItems.push(item);
        $scope.deleteIndex++;
        var index = $scope.items.indexOf(item);
        item.status = '80';
        $scope.items.splice(index, 1, item);
        $rootScope.getItemStore().put(item);
        $http.post('/api/einkaufsliste/remove_item', JSON.stringify(item)).then(
            function (resp) {
                $mdToast.show({
                    hideDelay: 0,
                    position: 'bottom right',
                    controller: 'ToastCtrl',
                    controllerAs: 'ctrl',
                    bindToController: true,
                    locals: {},
                    templateUrl: '/app/einkaufsliste/templates/undo_toast.html'
                });
            }
        )
    };

    $scope.checkItem = function (item) {
        var idx = $scope.items.indexOf(item);
        if (item.status === '50') {
            item.status = '10';
            item.checked = false;
        } else {
            item.status = '50';
            item.checked = true;
        }
        $rootScope.getItemStore().put(item);
        $http.post('/api/einkaufsliste/check_item', JSON.stringify(item)).then(
            function (resp) {
                $scope.items.splice(idx, 1, item);
            }
        );
    };

    $scope.$on('undoAction', function (event, data) {
        var item = $scope.deletedItems[$scope.deleteIndex];
        $scope.deletedItems.splice($scope.deleteIndex, 1);
        $scope.deleteIndex--;
        var index = $scope.items.indexOf(item);
        item.status = '10';
        item.checked = false;
        $scope.items.splice(index, 1, item);
        $rootScope.getItemStore().put(item);
        $http.post('/api/einkaufsliste/add_item', JSON.stringify(item)).then(
            function success(resp) {
            }, function error(resp) {
                console.log('ERROR: ' + JSON.parse(resp))
            }
        )
    });

    $scope.addItemPopup = function (ev) {
        var confirm = $mdDialog.prompt()
            .title('Item hinzufügen')
            .targetEvent(ev)
            .required(true)
            .ok('Okay!')
            .cancel('Abbrechen');

        $mdDialog.show(confirm).then(function (result) {
            var newItem = {einkaufslisteId: $routeParams.listeId, item: result, status: '10', timeAen: new Date(0)};
            $scope.items.push(newItem);
            console.log('after push');
            $rootScope.getItemStore().put(newItem);
            console.log('after put');
            $http.post('/api/einkaufsliste/add_item', {einkaufslisteId: $routeParams.listeId, item: result}).then(
                function (resp) {
                    $http.post('/api/einkaufsliste/get_item', {
                        einkaufslisteId: $routeParams.listeId,
                        item: result
                    }).then(
                        function (resp) {
                            $scope.items.some(function (item) {
                                if (item.item === resp.data.item) {
                                    $scope.items.splice($scope.items.indexOf(item), 1, resp.data);
                                    $rootScope.getItemStore().put(item);
                                    return true;
                                }
                            })
                        }
                    )
                }
            )
        }, function () {
        });
    }
});
eggiApp.controller('ReminderController', function ($scope, $rootScope, $http, $location, $mdDialog) {

    $scope.dates = [];

    $rootScope
        .getDateStore()
        .getAll()
        .onsuccess = function (event) {
        $scope.dates = event.target.result;
        $scope.dates.forEach(function (date) {
            date.datum = new Date(date.datum);
            date.timeAen = new Date(date.timeAen);
            date.isOnline = false;
            $scope.dates.splice($scope.dates.indexOf(date), 1, date);
        });
        $http.post('/api/reminder/get_all_dates').then(
            function succ(resp) {
                var datesOnline = resp.data;
                datesOnline.forEach(function (dateOnline) {
                    dateOnline.timeAen = new Date(dateOnline.timeAen);
                    var dateAlreadyOffline = false;
                    $scope.dates.some(function (dateOffline) {
                        if (dateOnline.id === dateOffline.id) {
                            dateOffline.isOnline = true;
                            dateAlreadyOffline = true;
                            if (dateOnline.timeAen < dateOffline.timeAen) {
                                console.log("SYNC push to Server!");
                                $http.post('/api/reminder/edit_or_add_date', JSON.stringify(dateOffline));
                            } else if (dateOffline.timeAen < dateOnline.timeAen) {
                                console.log("SYNC Push to Local!");
                                $rootScope
                                    .getDateStore()
                                    .put(dateOnline);
                            }
                            $scope.dates.splice($scope.dates.indexOf(dateOffline), 1, dateOffline);
                            return true;
                        }
                    });
                    if (!dateAlreadyOffline) {
                        $scope.dates.push(dateOnline);
                        $rootScope
                            .getDateStore()
                            .put(dateOnline);
                    }
                });
                $scope.dates.forEach(function (date) {
                    if (!date.isOnline) {
                        console.log("SYNC psuh to Server II!");
                        $http.post('/api/reminder/edit_or_add_date', JSON.stringify(date)).then(
                            function (resp) {
                                $rootScope
                                    .getDateStore()
                                    .delete(date.id);
                                $rootScope
                                    .getDateStore()
                                    .put(resp.data.date);
                            }
                        );
                    }
                });
            }
        );
    };

    $scope.editOrAddDatePopup = function (ev, date) {
        var idx = $scope.dates.indexOf(date);
        if (idx > -1) {
            date.uhrzeit = date.datum;
            date.uhrzeit.setMilliseconds(0);
            $rootScope.dateToEdit = date;
        }
        $mdDialog.show({
            controller: 'EditOrAddDateController',
            templateUrl: 'reminder/templates/datepopup.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: true // Only for -xs, -sm breakpoints.
        })
            .then(function (date) {
                console.log("Received: " + JSON.stringify(date));
                console.log("IDX: " + idx);
                if (idx === -1) {
                    date.id = new Date().getTime() * -1;
                    $scope.dates.push(date);
                } else
                    $scope.dates.splice(idx, 1, date);
                console.log('DS Call');
                $rootScope
                    .getDateStore()
                    .put(date)
                    .onsuccess = function () {
                    console.log("DS Call finished");
                    $http.post('/api/reminder/edit_or_add_date', JSON.stringify(date)).then(
                        function success(resp) {
                            $http.post('/api/reminder/get_date_by_id', JSON.stringify(resp.data.date)).then(
                                function success(resp) {
                                    if (idx === -1) {
                                        $rootScope
                                            .getDateStore()
                                            .delete(date.id); //DMY-Wert loeschen
                                        console.log('POST Delete DS');
                                    }
                                    $rootScope
                                        .getDateStore()
                                        .put(resp.data);
                                    $scope.dates.splice($scope.dates.indexOf(date), 1, resp.data);
                                    console.log('POST Push to DS');
                                }
                            );
                        }
                    );
                };
            }, function(){
                // Cancel!
            });
    };

    $scope.deleteDate = function (date) {
        $scope.dates.splice($scope.dates.indexOf(date), 1);
        $rootScope
            .getDateStore()
            .delete(date.id);
        $http.post('/api/reminder/delete_date', JSON.stringify(date));
    }

});
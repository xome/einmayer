eggiApp.controller('EditOrAddDateController', function($scope, $mdDialog, $rootScope){

    $scope.date = $rootScope.dateToEdit;
    if (typeof $scope.date === 'undefined')
        $scope.date = {};

    $scope.save = function(ev){
        $scope.date.datum.setHours($scope.date.uhrzeit.getHours());
        $scope.date.datum.setMinutes($scope.date.uhrzeit.getMinutes());
        $scope.date.datum.setSeconds($scope.date.uhrzeit.getSeconds());
        console.log("Save: " + JSON.stringify($scope.date));
        $mdDialog.hide($scope.date);
    };

    $scope.cancel = function(ev){
        $mdDialog.cancel(ev);
    };


    $scope.formatPretty = function(time){
        var date = new Date(parseInt(time));
        return date.toLocaleTimeString(navigator.language, {
            hour: '2-digit',
            minute:'2-digit'
        });
    }

});
const eggiApp = angular.module('eggiApp', ['ngRoute', 'ngMaterial', 'ngMessages'])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/einkaufsliste/', {
                templateUrl: 'einkaufsliste/templates/overview.html',
                controller: 'EinkaufslisteOverviewController',
            })
            .when('/einkaufsliste/:listeId', {
                templateUrl: 'einkaufsliste/templates/einkaufsliste.html',
                controller: 'EinkaufslisteController'
            })
            .when('/home', {
                templateUrl: 'templates/home.html',
                controller: 'HomeController'
            })
            .when('/reminder', {
                templateUrl: 'reminder/templates/reminder.html',
                controller: 'ReminderController'
            })
            .otherwise('/home');

        $locationProvider.html5Mode(true);

    });



eggiApp.controller('HomeController', function ($scope, $rootScope, $http, $location) {

    $scope.loadingLetzteListe = true;
    $scope.items = [];

    $http.post('/api/einkaufsliste/get_letzte_liste').then(
        function success(resp) {
            $scope.letzteListe = resp.data;
            $http.post('/api/einkaufsliste/get_posis_by_liste', JSON.stringify($scope.letzteListe)).then(
                function success(resp){
                    $scope.items = resp.data;
                    $scope.items.forEach(function(item){
                        item.checked = item.status==='50';
                    });
                    $scope.loadingLetzteListe = false;
                }, function err(resp){
                    $scope.loadingLetzteListe = false;
                }
            )
        }
    );
    $scope.loadingReminderHeute = true;
    $scope.dates = [];
    $http.post('/api/reminder/get_fuer_heute').then(
        function success(resp){
            resp.data.forEach(function(date){
               date.datum = new Date(date.datum);
               $scope.dates.push(date);
            });
            $scope.loadingReminderHeute = false;
        }, function error(resp){
            $scope.dates.push({titel: 'Fehler bei Verbindung zu Server'});
            $scope.loadingReminderHeute = false;
        }
    );

    $scope.jumpToReminder = function(){
        $location.path("reminder");
    };


    $scope.jumpToLastListe = function () {
        if (typeof $scope.letzteListe !== 'undefined')
            $location.path('/einkaufsliste/' + $scope.letzteListe.id);
    };


});
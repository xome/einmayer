eggiApp.controller('mainController', function ($scope, $location, $rootScope) {
    $scope.setPathToEinkaufsliste = function () {
        $location.path("einkaufsliste")
    };
    $scope.setPathToHome = function() {
        $location.path("/");
    };
    $scope.setPathToReminder = function(){
        $location.path("reminder");
    };

    var dbRequest = indexedDB.open('eggi', 8);

    dbRequest.onupgradeneeded = function () {
        $rootScope.db = this.result;
        if (!$rootScope.db.objectStoreNames.contains('einkaufsliste'))
            $rootScope.db.createObjectStore('einkaufsliste', {
                keyPath: "id", autoIncrement: false
            });
        if (!$rootScope.db.objectStoreNames.contains("einkaufsliste_item")) {
            var store = $rootScope.db.createObjectStore("einkaufsliste_item", {
                keyPath: ["einkaufslisteId", "item"]
            });
            store
                .createIndex('einkaufsliste', 'einkaufslisteId', {unique: false});
        }
        if (!$rootScope.db.objectStoreNames.contains("date")){
            $rootScope.db.createObjectStore('date', {keyPath: "id"});
        }
    };

    dbRequest.onsuccess = function () {
        $rootScope.db = this.result;
    };

    $rootScope.getOverviewTx = function () {
        return $rootScope.db
            .transaction(['einkaufsliste'], 'readwrite')
            .objectStore('einkaufsliste');
    };

    $rootScope.getItemStore = function () {
        console.log('ITEMSTORE called');
        return $rootScope.db
            .transaction(["einkaufsliste_item"], 'readwrite')
            .objectStore("einkaufsliste_item");
    };

    $rootScope.getDateStore = function(){
        return $rootScope
            .db
            .transaction(["date"], "readwrite")
            .objectStore("date");
    }

});
const mainApp = angular.module('mainApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'luegg.directives'])
    .config(function($routeProvider, $locationProvider, $mdThemingProvider, $httpProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: 'templates/home.html',
                controller: 'HomeController'
            })
            .when('/wizard', {
                templateUrl: 'templates/wizardlobby.html',
                controller: 'WizardLobbyController'
            })
            .when('/wizard/game/:id', {
                templateUrl: 'templates/wizardgame.html',
                controller: 'WizardGameController'
            })
            .otherwise('/home');

        $locationProvider.html5Mode(true);

        $mdThemingProvider
            .theme('default')
            .dark();

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    });



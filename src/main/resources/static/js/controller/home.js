mainApp.controller('HomeController', function ($scope, $rootScope, $http, $location) {
    $scope.angemeldet = false;
    $scope.loading = false;
    $scope.user = new User();

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }



    $scope.anmelden = function () {
        $http.post('/login', "username=" + $scope.user.name + "&password=" + $scope.user.password,
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(
                function success(resp) {
                    $scope.angemeldet = true;
                    document.cookie = "username=" + $scope.user.name+"; password="+$scope.user.password+"; expires=Sat, 18 Dec 2100 12:00:00 UTC";
                    document.cookie = "password=" + $scope.user.password+"; expires=Sat, 18 Dec 2100 12:00:00 UTC";
                    $rootScope.user = $scope.user;
                    $location.path('wizard');
                }, function error(resp) {
                    $scope.loading = false;
                    $scope.loginForm.name.$valid = false;
                    $scope.loginForm.name.$error.pattern = true;
                    $scope.angemeldet = false;
                    if (resp.status === 403) {
                        $scope.errormsg = resp.data;
                    } else if (resp.status === 401) {
                        $scope.errormsg = "Zugangsdaten sind falsch!";
                    } else {
                        $scope.errormsg = "Unbekannter Fehler!";
                        console.error(resp);
                    }
                }
            )
    };
    if(document.cookie){
        $scope.loading = true;
        $scope.user.name = getCookie("username");
        $scope.user.password = getCookie("password");
        $scope.anmelden();
    }

});
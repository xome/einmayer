mainApp.controller('WizardLobbyController', function ($scope, $rootScope, $http, $location) {
    $scope.chatmsg = "waddupp";
    $scope.games = {};
    $scope.chatverlauf = [];
    $scope.loading = true;


    var subscriptionLobby = null;
    var socket = new SockJS('/ws');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);

    if (!$rootScope.user){
        $http.get('/get_user').then(function(resp){
            $rootScope.user = resp.data;
            getOpenGames($http, $scope, subscriptionLobby, $location, $rootScope, stompClient);
        });
    } else {
        getOpenGames($http, $scope, subscriptionLobby, $location, $rootScope, stompClient);
    }


    $scope.interpretiereKeypress = function($event){
        if ($event.key ==='Enter'){
            $scope.sendeChatMsg();
        }
    };

    $scope.sendeChatMsg = function(){
        console.log("CHAT: " + $scope.chatmsg);
        let msg = new LobbyMessage(LOBBY_MESSAGE_TYPE_CHAT, {message: $scope.chatmsg, recipients: []}, $rootScope.user.name);
        stompClient.send('/wizard/lobby.chat', {}, JSON.stringify(msg));
        $scope.chatmsg = "";
    }

    $scope.neuesSpiel = function(){
        createNewGame(stompClient, $rootScope, subscriptionLobby);
    };

    $scope.joinGame = function(game){
        joinGame(game, subscriptionLobby, $location, $scope, stompClient);
    };

    function onConnected() {
        subscriptionLobby = stompClient.subscribe('/lobby/public', onMessageReceived);
    }

    function onError() {
        $scope.msg = "Fehler beim Verbinden mit WebSocket!";
    }

    function onMessageReceived(payload) {
        console.log("Message received!" + JSON.stringify(new Date()));
        let msg = JSON.parse(payload.body);
        if (msg.msgType === LOBBY_MESSAGE_TYPE_CREATE_GAME){
            console.log(JSON.stringify(msg.content));
            gameWasCreated(msg.content.wizardGame, msg.content.creator, $rootScope, $scope, subscriptionLobby, $location, stompClient);
        } else if (msg.msgType === LOBBY_MESSAGE_TYPE_CHAT){
            chatMsgReceived($scope, msg);
        } else if (msg.msgType === LOBBY_MESSAGE_TYPE_CLOSED_GAME) {
            gameWasClosed($scope, msg);
        }
    }

});
mainApp.controller('WizardGameController', function($scope, $rootScope, $http, $location, $route, $mdDialog){
    $scope.idGame = $route.current.params['id'];
    $scope.newChatMessage = false;
    $scope.voteIsNeeded = false;
    $scope.chatverlauf = [];
    $scope.seats = {};
    $scope.ownCards = [];
    $scope.tableDom = angular.element(document.getElementById("table"))[0];
    $scope.functionsListIsOpen = false;
    $scope.gameIsReadyToStart = false;

    var socket = new SockJS('/ws');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, onConnected, onError);
    var subscription = null;

    $scope.mapColorIdToCssClass = { 'ro': 'number-red',
        'ge': 'number-yellow',
        'gr': 'number-green',
        'bl': 'number-blue'
    };

    $scope.ownCards.push(new WizardKarte('1', 'ro', '13', '/img/13_red.png'));

    function onConnected() {
        subscription = stompClient.subscribe('/lobby/games/' + $route.current.params.id, onMessageReceived);
        let msg = new GameMessage($scope.idGame, GameMessageType.JOIN, $rootScope.user.name);
        stompClient.send('/wizard/game.join', {}, JSON.stringify(msg));
    }

    function onMessageReceived(payload){
        let msg = JSON.parse(payload.body);
        if (msg.msgType === GameMessageType.CHAT){
            if (!$scope.chatIsOpen){
                $scope.primaryOrWarn = "md-warn";
            }
            chatMsgReceived($scope, msg);
        } else if (msg.msgType === GameMessageType.JOIN){
            userJoinedGame($scope, msg, $rootScope.user.name, $http);
        } else if (msg.msgType === GameMessageType.LEAVE){
            userLeftGame($scope, msg, $rootScope.user.name, $location, subscription, stompClient);
        } else if (msg.msgType === GameMessageType.GAME_IS_READY_TO_START){
            $scope.gameIsReadyToStart = true;
            $scope.voteIsNeeded = true;
        }
    }

    function onError() {
        $scope.msg = "Fehler beim Verbinden mit WebSocket!";
    }

    if (!$rootScope.user){
        $http.get('/get_user').then(function(resp){$rootScope.user = resp.data});
    }


    $scope.openChat = function(ev){
        $scope.chatIsOpen = true;
        $scope.newChatMessage = false;
        $mdDialog.show({
            contentElement: '#chatDialog',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function(){
            $scope.chatIsOpen = false;
        }, function(){$scope.chatIsOpen = false;});
    };

    $scope.closeDialog = function(){
        $scope.chatIsOpen = false;
        $mdDialog.hide();
    };

    $scope.interpretiereKeypress = function($event){
        if ($event.key ==='Enter'){
            $scope.sendeChatMsg();
        }
    };

    $scope.sendeChatMsg = function(){
        if ($scope.chatmsg === '' || typeof $scope.chatmsg === 'undefined') return;
        let msg = new GameMessage($scope.idGame, GameMessageType.CHAT, $rootScope.user.name, {message: $scope.chatmsg, idGame: $route.current.params.id});
        stompClient.send('/wizard/game.chat', {}, JSON.stringify(msg));
    }

    $scope.leaveGame = function(ev){
        let msg = new GameMessage($scope.idGame, GameMessageType.LEAVE, $rootScope.user.name);
        stompClient.send('/wizard/game.leave', {}, JSON.stringify(msg));
    }




});
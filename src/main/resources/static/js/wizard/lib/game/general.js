function userJoinedGame($scope, msg, ownUser, $http) {
    if (msg.sender === ownUser) {
        $scope.ownSeat = msg.seat;
        $http.get('/wizard/game/get_other_participants',
            {idGame: $scope.idGame, ownUser: ownUser})
            .then(function succ(resp) {
                resp.data.forEach(function (participant) {
                    addParticipant(participant, $scope);
                });
            }, function err(resp) {
                console.error(resp);
            });
    } else {
        addParticipant(new WizardGameParticipant($scope.idGame, msg.sender, msg.seat), $scope);
    }
    
    $scope.$apply();
}

function addParticipant(participantPara, $scope) {
    let participant =
        new WizardGameParticipant(participantPara.idGame,
            participantPara.nameUser,
            participantPara.seat);

    $scope.seats[participant.seat] = participant.nameUser;
}

function userLeftGame($scope, msg, ownName, $location, subscription, stomp){
    if (msg.sender === ownName){
        subscription.unsubscribe();
        stomp.disconnect();
        $location.path('/wizard/lobby');
        $scope.$apply();
    }
}
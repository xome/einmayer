function joinGame(wizardGame, subscriptionLobby, $location, $scope, stomp){
    if (subscriptionLobby) {
        subscriptionLobby.unsubscribe();
        stomp.disconnect();
    }

    $location.path('/wizard/game/' + wizardGame.id);
    $scope.$apply();
    console.log("Path set!" + JSON.stringify(new Date()));
}


function gameWasCreated(wizardGame, creator, $rootScope, $scope, subscriptionLobby, $location, stomp){
    if (creator.name === $rootScope.user.name){
        joinGame(wizardGame, subscriptionLobby, $location, $scope, stomp);
    } else {
        let lst = [];
        let participant = new WizardGameParticipant(wizardGame.gameId, creator.name, 1, WizardGameParticipantStatus.IN_GAME);
        lst.push(participant);
        let obj = {wizardGame: wizardGame, participants: lst};
        $scope.games.push(obj);
        $scope.$apply();
    }
}

function createNewGame(stompClient, $rootScope){
    let msg = new LobbyMessage(LOBBY_MESSAGE_TYPE_CREATE_GAME, {}, $rootScope.user.name);
    stompClient.send('/wizard/game.create', {}, JSON.stringify(msg));
    console.log("Message sent!" + JSON.stringify(new Date()));
}

function getOpenGames($http, $scope, subscriptionLobby, $location, $rootScope, stomp) {
    $http.get('/wizard/get_open_games').then(function (resp) {
        $scope.games = resp.data;
        $scope.games.forEach(function (gameWithParticipants) {
            gameWithParticipants.participants.forEach(function (participant) {
                if (participant.nameUser === $rootScope.user.name
                    && (participant.stat === WizardGameParticipantStatus.IN_GAME
                    || participant.stat === WizardGameParticipantStatus.DROPPED)) {
                    joinGame(gameWithParticipants.wizardGame, subscriptionLobby, $location, $scope, stomp);
                }
            })
        });
        $scope.loading = false;
    });
}

function gameWasClosed($scope, msg){
    $scope.games.some(function(obj){
        let game = obj.wizardGame;
        if (game.id === msg.game.id){
            $scope.games.splice(obj, 1);
            return true;
        }
    });
    $scope.$apply();
}
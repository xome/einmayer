function chatMsgReceived($scope, msg){
    if ($scope.chatverlauf.length > 250)
        $scope.chatverlauf.splice(0,1);
    $scope.chatverlauf.push({sender: msg.sender, msg: msg.content.message});
    $scope.$apply();

}
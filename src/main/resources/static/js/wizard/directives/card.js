mainApp.directive('card', function () {
    return {
        templateUrl: '/js/wizard/directives/templates/card.html',
        restrict: 'E',
        scope: {
            imgUrl: "=",
            number: "=",
            colorClass: "=",
            rotation: "=",
            cardWidth: "=",
        }, link: function (scope) {
            // ursprüngliche Größe: 186px * 287px
            let ratio = 1.543010752688172043010752688172;
            let height = (scope.cardWidth * ratio) + "px";
            let width = scope.cardWidth + "px";
            scope.myStyle = {
                "background-image": "url(" + scope.imgUrl + ")",
                "background-size": "cover",
                "width": width,
                "height": height,
                "rotation": scope.rotation + "deg"
            };
        }
    }
})
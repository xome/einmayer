mainApp.directive('numberSeven', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/7.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
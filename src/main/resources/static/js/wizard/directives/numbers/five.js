mainApp.directive('numberFive', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/5.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
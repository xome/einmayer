mainApp.directive('numberTen', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/10.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
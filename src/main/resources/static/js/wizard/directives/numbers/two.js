mainApp.directive('numberTwo', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/2.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
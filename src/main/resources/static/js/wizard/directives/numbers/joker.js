mainApp.directive('numberJoker', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/N.svg',
        replace: true
    };
});
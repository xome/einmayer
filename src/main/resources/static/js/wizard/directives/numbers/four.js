mainApp.directive('numberFour', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/4.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
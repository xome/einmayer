mainApp.directive('numberSix', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/6.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
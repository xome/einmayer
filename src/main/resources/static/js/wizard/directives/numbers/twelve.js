mainApp.directive('numberTwelve', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/12.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
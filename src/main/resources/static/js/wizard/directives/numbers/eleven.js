mainApp.directive('numberEleven', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/11.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
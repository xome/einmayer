mainApp.directive('numberEight', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/8.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
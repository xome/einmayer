mainApp.directive('numberOne', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/1.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
mainApp.directive('numberThirteen', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/13.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
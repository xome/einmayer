mainApp.directive('numberThree', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/3.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
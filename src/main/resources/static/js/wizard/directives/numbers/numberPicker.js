mainApp.directive('numberPicker', function () {
    return {
        templateUrl: '/js/wizard/directives/templates/numberPicker.html',
        scope: {
            number: "=",
            colorClass: "=",
            numberWidth: "=",
        },
        replace: true,
        restrict: 'E',
        link: function (scope) {
            scope.numberStyle = {
                "width": scope.numberWidth + "px",
                "height": scope.numberWidth + "px"
            };
        }
    }
})
mainApp.directive('numberNine', function(){
    return {
        restrict: 'E',
        templateNamespace: 'svg',
        templateUrl: 'img/9.svg',
        replace: true,
        scope: {
            color: "="
        }
    };
});
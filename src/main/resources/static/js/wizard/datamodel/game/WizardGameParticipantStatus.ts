class WizardGameParticipantStatus {
    static IN_GAME: string = 'IN_GAME';
    static DROPPED: string = 'DROPPED';
}

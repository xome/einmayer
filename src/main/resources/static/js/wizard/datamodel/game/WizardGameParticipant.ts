class WizardGameParticipant {
    idGame:number;
    nameUser:string;
    seat: number;
    stat: string;

    constructor(idGame:number, nameUser:string, seat:number, stat:string) {
        this.idGame = idGame;
        this.nameUser = nameUser;
        this.seat = seat;
        this.stat = stat;
    }
}
class WizardKarte {

    private _id: string;
    private _idFarbe: string;
    private _idZahl: string;
    private _imgPath: string;


    constructor(id: string, idFarbe: string, idZahl: string, imgPath: string) {
        this._id = id;
        this._idFarbe = idFarbe;
        this._idZahl = idZahl;
        this._imgPath = imgPath;
    }

    public initFromJson(json:string){
        let obj=JSON.parse(json);
        return new WizardKarte(obj.id, obj.idFarbe, obj.idZahl, obj.imgPath);
    }


    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get idFarbe(): string {
        return this._idFarbe;
    }

    set idFarbe(value: string) {
        this._idFarbe = value;
    }

    get idZahl(): string {
        return this._idZahl;
    }

    set idZahl(value: string) {
        this._idZahl = value;
    }
    get imgPath(): string {
        return this._imgPath;
    }

    set imgPath(value: string) {
        this._imgPath = value;
    }
}
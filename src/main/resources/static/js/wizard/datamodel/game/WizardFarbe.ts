class WizardFarbe {

    private _id: string;

    constructor(id: string){
        this._id = id;
    }

    initFromJson(json:string){
        let obj = JSON.parse(json);
        return new WizardFarbe(obj.id);
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }
}
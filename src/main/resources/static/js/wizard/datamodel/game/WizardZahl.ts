class WizardZahl {

    private _id: string;

    constructor(id: string) {
        this._id = id;
    }

    public initFromJson(json: string){
        let obj = JSON.parse(json);
        return new WizardZahl(obj.id);
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }
}
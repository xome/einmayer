class User {
    name;
    password;
    rolle;
    loginCookieKey;

    constructor(name, password, rolle, loginCookieKey) {
        this.name = name;
        this.password = password;
        this.rolle = rolle;
        this.loginCookieKey = loginCookieKey;
    }
}
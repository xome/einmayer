class LobbyMessage extends Message {
    msgType;

    constructor(msgType, content, sender) {
        super(sender, new Date(), undefined, content, '/lobby');
        this.msgType = msgType;
    }
}

class GameMessage extends Message {

    gameId;
    msgType;
    sender;
    content;

    constructor(gameId, messageType, sender, content) {
        super(sender, new Date(), undefined, content);
        this.gameId = gameId;
        this.msgType = messageType;
        this.sender = sender;
        this.content = content;
    }
}
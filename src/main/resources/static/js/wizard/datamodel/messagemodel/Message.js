class Message {
    sender;
    messageSentAt;
    messageReceivedByServerAt;
    content;
    receiverChannel;

    constructor(sender, messageSentAt, messageReceivedByServerAt, content, receiverChannel) {
        this.sender = sender;
        this.messageSentAt = messageSentAt;
        this.messageReceivedByServerAt = messageReceivedByServerAt;
        this.content = content;
        this.receiverChannel = receiverChannel;
    }
}

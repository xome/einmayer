class GameClosedMessage extends Message {

    game;
    msgType;


    constructor(sender, messageSentAt, messageReceivedByServerAt, content, receiverChannel, game, msgType) {
        super(sender, messageSentAt, messageReceivedByServerAt, content, receiverChannel);
        this.game = game;
        this.msgType = msgType;
    }
}
package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.wizard.socket.model.Message;

import java.util.Objects;

public class GameChatMessage extends Message {

    private GameChatMessageContent content;
    private GameMessageType msgType;

    public GameChatMessage(String sender, GameChatMessageContent content) {
        super(sender);
        this.content = content;
        this.msgType = GameMessageType.CHAT;
    }

    @Override
    public String toString() {
        return "GameChatMessage{" +
                "content=" + content +
                ", msgType=" + msgType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GameChatMessage that = (GameChatMessage) o;
        return Objects.equals(content, that.content) &&
                msgType == that.msgType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), content, msgType);
    }

    public GameMessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(GameMessageType msgType) {
        this.msgType = msgType;
    }

    public GameChatMessageContent getContent() {
        return content;
    }

    public void setContent(GameChatMessageContent content) {
        this.content = content;
    }
}

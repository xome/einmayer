package de.mayer.einmayer.webserver.wizard.http;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.dao.UserRepo;
import de.mayer.einmayer.webserver.dao.wizard.*;
import de.mayer.einmayer.webserver.wizard.lobby.LobbyLogic;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.GameWithParticipants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
public class LobbyHttpController {

    private WizardGameRepo wizardGameRepo;
    private UserRepo userRepo;
    private WizardGameParticipantRepo wizardGameParticipantRepo;
    private LobbyLogic lobbyLogic;

    @Autowired
    public void setLobbyLogic(LobbyLogic lobbyLogic) {
        this.lobbyLogic = lobbyLogic;
    }

    @Autowired
    public void setWizardGameRepo(WizardGameRepo wizardGameRepo) {
        this.wizardGameRepo = wizardGameRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setWizardGameParticipantRepo(WizardGameParticipantRepo wizardGameParticipantRepo) {
        this.wizardGameParticipantRepo = wizardGameParticipantRepo;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/wizard/get_open_games")
    public @ResponseBody
    List<GameWithParticipants> getOpenGames() {
        List<GameWithParticipants> ret = new ArrayList<>();
        Set<WizardGame> games = new HashSet<>(wizardGameRepo.findByRoundAndStat(0,
                WizardGameStat.OPEN.toString()));
        games.forEach(game -> {
            GameWithParticipants retItem = new GameWithParticipants();
            retItem.setWizardGame(game);
            retItem.setParticipants(
                    wizardGameParticipantRepo
                            .findByIdGameAndStatIn(game.getId(),
                                    Arrays.asList(WizardGameParticipantStat.IN_GAME.toString(),
                                            WizardGameParticipantStat.DROPPED.toString())));
            ret.add(retItem);
        });
        return ret;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/wizard/leave_lobby")
    public void leaveLobby(@RequestBody User user) {
        lobbyLogic.userLeft(user.getName());
    }


}

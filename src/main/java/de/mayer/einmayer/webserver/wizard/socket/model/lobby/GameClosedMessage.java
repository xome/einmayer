package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.wizard.socket.model.Message;

import java.util.Objects;

public class GameClosedMessage extends Message {

    private final WizardGame game;
    private final LobbyMessageType msgType;

    public GameClosedMessage(String sender, WizardGame game) {
        super(sender);
        this.game = game;
        msgType = LobbyMessageType.CLOSED_GAME;
    }


    public WizardGame getGame() {
        return game;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GameClosedMessage that = (GameClosedMessage) o;
        return Objects.equals(game, that.game);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), game);
    }

    public LobbyMessageType getMsgType() {
        return msgType;
    }
}

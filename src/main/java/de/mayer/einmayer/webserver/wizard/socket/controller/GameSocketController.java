package de.mayer.einmayer.webserver.wizard.socket.controller;

import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.dao.wizard.WizardGameParticipant;
import de.mayer.einmayer.webserver.dao.wizard.WizardGameRepo;
import de.mayer.einmayer.webserver.wizard.game.GameLogic;
import de.mayer.einmayer.webserver.wizard.lobby.LobbyLogic;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameChatMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameJoinOrLeaveMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

@Controller
public class GameSocketController {


    @MessageMapping("game.playCard")
    @SendTo("/games")
    public GameMessage sendMessage(@Payload GameMessage gameMessage) {
        return gameMessage;
    }

    @MessageMapping("game.chat")
    public void chat(@Payload GameChatMessage message){
        WizardGame wizardGame = wizardGameRepo.findById(message.getContent().getIdGame()).orElse(null);
        if (wizardGame == null) return;
        message.setSender(SecurityContextHolder.getContext().getAuthentication().getName());
        messageOperations.convertAndSend(String.format("/lobby/games/%d", wizardGame.getId()), message);
    }

    @MessageMapping("game.join")
    public void joinGame(@Payload GameJoinOrLeaveMessage gameMessage) {
        gameMessage.setSender(SecurityContextHolder.getContext().getAuthentication().getName());
        WizardGame game = wizardGameRepo.findById(gameMessage.getGameId()).orElse(null);
        if (game == null) return;

        gameLogic.userJoined(game, gameMessage.getSender());

    }

    @MessageMapping("game.leave")
    public void leaveGame(@Payload GameJoinOrLeaveMessage message){
        message.setSender(SecurityContextHolder.getContext().getAuthentication().getName());
        gameLogic.userLeft(message.getGameId(), message.getSender());
    }

    private LobbyLogic lobbyLogic;

    @Autowired
    public void setLobbyLogic(LobbyLogic lobbyLogic) {
        this.lobbyLogic = lobbyLogic;
    }

    private GameLogic gameLogic;

    @Autowired
    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    private WizardGameRepo wizardGameRepo;

    @Autowired
    public void setWizardGameRepo(WizardGameRepo wizardGameRepo) {
        this.wizardGameRepo = wizardGameRepo;
    }

    private SimpMessageSendingOperations messageOperations;

    @Autowired
    public void setMessageOperations(SimpMessageSendingOperations messageOperations) {
        this.messageOperations = messageOperations;
    }
}

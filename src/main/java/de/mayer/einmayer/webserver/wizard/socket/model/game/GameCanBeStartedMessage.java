package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.wizard.socket.model.Message;

public class GameCanBeStartedMessage extends Message {
    public GameCanBeStartedMessage(String sender) {
        super(sender);
        msgType = GameMessageType.GAME_IS_READY_TO_START;
    }

    private GameMessageType msgType;

    public GameMessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(GameMessageType msgType) {
        this.msgType = msgType;
    }
}

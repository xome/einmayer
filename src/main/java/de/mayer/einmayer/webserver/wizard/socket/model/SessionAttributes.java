package de.mayer.einmayer.webserver.wizard.socket.model;

import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SessionAttributes {

    private String name;
    private Integer gameId;

    public SessionAttributes(Map<String, Object> websocketSessionAttributes) {
        if (websocketSessionAttributes == null)
            return;
        this.name = (String) websocketSessionAttributes.get("name");
        this.gameId = (Integer) websocketSessionAttributes.get("gameId");
    }

    public SimpMessageHeaderAccessor updateWebsocketSessionAttributes(SimpMessageHeaderAccessor headerAccessor){
        if (headerAccessor.getSessionAttributes() == null){
            headerAccessor.setSessionAttributes(new HashMap<>());
        }
        headerAccessor.getSessionAttributes().put("name", getName());
        headerAccessor.getSessionAttributes().put("gameId", getGameId());
        return headerAccessor;
    }

    @Override
    public String toString() {
        return "SessionAttributes{" +
                "name='" + name + '\'' +
                ", gameId=" + gameId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionAttributes that = (SessionAttributes) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(gameId, that.gameId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, gameId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }
}

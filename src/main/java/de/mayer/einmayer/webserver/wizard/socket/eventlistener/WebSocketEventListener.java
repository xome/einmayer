package de.mayer.einmayer.webserver.wizard.socket.eventlistener;

import com.mysql.cj.Session;
import de.mayer.einmayer.webserver.wizard.game.GameLogic;
import de.mayer.einmayer.webserver.wizard.lobby.LobbyLogic;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameMessageType;
import de.mayer.einmayer.webserver.wizard.socket.model.SessionAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Date;

@Component
public class WebSocketEventListener {

    Logger log = LoggerFactory.getLogger(WebSocketEventListener.class);

    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    public void setMessagingTemplate(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent connectedEvent){
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(connectedEvent.getMessage());
        SessionAttributes sessionAttributes = new SessionAttributes(headerAccessor.getSessionAttributes());
        sessionAttributes.setName(connectedEvent.getUser().getName());
        sessionAttributes.updateWebsocketSessionAttributes(headerAccessor);
        //log.info("Verbindung wurde hergestellt.");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent disconnectEvent){
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(disconnectEvent.getMessage());
        SessionAttributes attributes = new SessionAttributes(headerAccessor.getSessionAttributes());

        if (attributes.getGameId() == null){
            lobbyLogic.userLeft(attributes.getName());
            gameLogic.userDropped(disconnectEvent.getUser().getName());
        }

        if (attributes.getName() != null){
            log.info(String.format("%s hat die Verbindung unterbrochen.", attributes.getName()));
        }
    }

    private GameLogic gameLogic;

    @Autowired
    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    private LobbyLogic lobbyLogic;

    @Autowired
    public void setLobbyLogic(LobbyLogic lobbyLogic) {
        this.lobbyLogic = lobbyLogic;
    }
}

package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.dao.wizard.WizardGameParticipant;

import java.util.List;
import java.util.Objects;

public class GameWithParticipants {
    private WizardGame wizardGame;
    private List<WizardGameParticipant> participants;


    @Override
    public String toString() {
        return "GameWithParticipants{" +
                "wizardGame=" + wizardGame +
                ", participants=" + participants +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameWithParticipants that = (GameWithParticipants) o;
        return Objects.equals(wizardGame, that.wizardGame) &&
                Objects.equals(participants, that.participants);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wizardGame, participants);
    }

    public WizardGame getWizardGame() {
        return wizardGame;
    }

    public void setWizardGame(WizardGame wizardGame) {
        this.wizardGame = wizardGame;
    }

    public List<WizardGameParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<WizardGameParticipant> participants) {
        this.participants = participants;
    }
}

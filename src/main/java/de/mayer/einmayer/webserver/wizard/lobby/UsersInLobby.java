package de.mayer.einmayer.webserver.wizard.lobby;

import de.mayer.einmayer.webserver.dao.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

@Component
@Scope("singleton")
public class UsersInLobby {

    private ConcurrentSkipListSet<User> usersInLobby;

    public UsersInLobby(){
        this.usersInLobby = new ConcurrentSkipListSet<>(Comparator.comparing(User::getName));
    }

    public void addUser(User user){
        usersInLobby.add(user);
    }

    public void removeUser(User user){
        usersInLobby.remove(user);
    }

    public Set<User> getUsers(){
        return new HashSet<>(usersInLobby);
    }

}

package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Objects;

public class CreateGameContent extends MessageContent {

    private WizardGame wizardGame;
    private User creator;

    public CreateGameContent(WizardGame wizardGame, User creator) {
        this.wizardGame = wizardGame;
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "CreateGameContent{" +
                "wizardGame=" + wizardGame +
                ", creator=" + creator +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateGameContent that = (CreateGameContent) o;
        return Objects.equals(wizardGame, that.wizardGame) &&
                Objects.equals(creator, that.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wizardGame, creator);
    }

    public WizardGame getWizardGame() {
        return wizardGame;
    }

    public void setWizardGame(WizardGame wizardGame) {
        this.wizardGame = wizardGame;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}

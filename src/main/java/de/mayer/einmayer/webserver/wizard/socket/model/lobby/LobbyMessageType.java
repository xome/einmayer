package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

public enum LobbyMessageType {

    JOIN,
    LEAVE,
    CREATE_GAME,
    CLOSED_GAME,
    CHAT

}

package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.wizard.socket.model.Message;
import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Objects;

public class SimpleLobbyMessage extends Message {

    private MessageContent content;
    private String sender;
    private LobbyMessageType msgType;

    public SimpleLobbyMessage(MessageContent content, String sender, LobbyMessageType msgType) {
        super(sender);
        this.content = content;
        this.sender = sender;
        this.msgType = msgType;
    }

    @Override
    public String toString() {
        return "LobbyCreateGameMessage{" +
                "content=" + content +
                ", sender='" + sender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SimpleLobbyMessage that = (SimpleLobbyMessage) o;
        return Objects.equals(content, that.content) &&
                Objects.equals(sender, that.sender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), content, sender);
    }

    public MessageContent getContent() {
        return content;
    }

    public void setContent(MessageContent content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }


    public LobbyMessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(LobbyMessageType msgType) {
        this.msgType = msgType;
    }
}

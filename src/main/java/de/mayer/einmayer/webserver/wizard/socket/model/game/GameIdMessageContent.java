package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Objects;

public class GameIdMessageContent extends MessageContent {

    private Integer id;

    public GameIdMessageContent(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "GameIdMessageContent{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameIdMessageContent that = (GameIdMessageContent) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

package de.mayer.einmayer.webserver.wizard.game;

import de.mayer.einmayer.webserver.dao.wizard.*;
import de.mayer.einmayer.webserver.wizard.socket.Channels;
import de.mayer.einmayer.webserver.wizard.socket.model.Message;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameCanBeStartedMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameJoinOrLeaveMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.game.GameMessageType;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.GameClosedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class GameLogic {
    public WizardGame newGame() {
        WizardGame game = new WizardGame();
        game.setRound(0);
        game.setStat(WizardGameStat.OPEN.toString());
        return wizardGameRepo.save(game);
    }

    public void userLeft(Integer gameId, String sender) {
        var id = new WizardGameParticipantId();
        id.setNameUser(sender);
        id.setIdGame(gameId);
        var participant = wizardGameParticipantRepo
                .findById(id)
                .orElse(null);
        if (participant == null) return;
        var anzTeilnehmerNochImSpiel = wizardGameParticipantRepo
                .countByIdGameAndStat(gameId,
                        WizardGameParticipantStat.IN_GAME.toString())
                - 1;

        var msg = new GameJoinOrLeaveMessage(sender, GameMessageType.LEAVE, gameId, new Date());
        msg.setSeat(participant.getSeat());
        messageOperations.convertAndSend(String.format("/lobby/games/%d", gameId), msg);
        messageOperations.convertAndSend(Channels.LOBBY + "/public", msg);

        if (anzTeilnehmerNochImSpiel > 0) {
            participant.setStat(WizardGameParticipantStat.LEFT.toString());
            wizardGameParticipantRepo.save(participant);
        } else {
            var game = wizardGameRepo.findById(gameId).orElse(null);
            if (game == null) return;

            game.setStat(WizardGameStat.CLOSED.toString());
            wizardGameRepo.save(game);
            var participants = wizardGameParticipantRepo
                                    .findAllByIdGame(gameId);

            participants.forEach(part -> part.setStat(WizardGameParticipantStat.LEFT.toString()));
            wizardGameParticipantRepo.saveAll(participants);
            var closedMsg = new GameClosedMessage(Message.SENDER_SERVER, game);
            messageOperations.convertAndSend(Channels.LOBBY + "/public", closedMsg);
        }


    }

    public void userJoined(WizardGame game, String username) {
        WizardGameParticipantId idPart = new WizardGameParticipantId();
        idPart.setIdGame(game.getId());
        idPart.setNameUser(username);
        WizardGameParticipant participant = wizardGameParticipantRepo
                .findById(idPart)
                .orElseGet(() -> {
                  WizardGameParticipant newParticipant = new WizardGameParticipant();
                  newParticipant.setNameUser(username);
                  newParticipant.setIdGame(game.getId());
                  newParticipant.setStat(WizardGameParticipantStat.IN_GAME.toString());
                  return newParticipant;
                });

        if (participant.getSeat() == null) {
            Long nextSeat = wizardGameParticipantRepo.maxByIdGame(game.getId());
            if (nextSeat == null) nextSeat = 1L;
            else nextSeat = nextSeat + 1;
            participant.setSeat(nextSeat.intValue());
            wizardGameParticipantRepo.save(participant);
        }

        GameJoinOrLeaveMessage joinMessage = new GameJoinOrLeaveMessage(Message.SENDER_SERVER, GameMessageType.JOIN, game.getId(), new Date());
        joinMessage.setSeat(participant.getSeat());
        messageOperations.convertAndSend(String.format("/lobby/games/%d", game.getId()), joinMessage);

        if (wizardGameParticipantRepo.countByIdGame(game.getId()).intValue() ==
                GameRules.MIN_PLAYERCOUNT_TO_START){
            GameCanBeStartedMessage msg = new GameCanBeStartedMessage(Message.SENDER_SERVER);
            messageOperations.convertAndSend(String.format("/lobby/games/%d", game.getId()), msg);
        }
    }

    public void userDropped(String name) {
        WizardGameParticipant participant = wizardGameParticipantRepo
                .findByNameUserAndStat(name,
                        WizardGameParticipantStat.IN_GAME.toString());
        if (participant == null) return;

        participant.setStat(WizardGameParticipantStat.DROPPED.toString());
        wizardGameParticipantRepo.save(participant);
        Message msg = new GameJoinOrLeaveMessage(name, GameMessageType.DROP, participant.getIdGame(), new Date());
        messageOperations.convertAndSend(String.format("lobby/games/%d", participant.getIdGame()), msg);
    }


    private WizardGameParticipantRepo wizardGameParticipantRepo;

    @Autowired
    public void setWizardGameParticipantRepo(WizardGameParticipantRepo wizardGameParticipantRepo) {
        this.wizardGameParticipantRepo = wizardGameParticipantRepo;
    }

    private SimpMessageSendingOperations messageOperations;

    @Autowired
    public void setMessageOperations(SimpMessageSendingOperations messageOperations) {
        this.messageOperations = messageOperations;
    }

    private WizardGameRepo wizardGameRepo;

    @Autowired
    public void setWizardGameRepo(WizardGameRepo wizardGameRepo) {
        this.wizardGameRepo = wizardGameRepo;
    }


}

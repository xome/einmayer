package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.wizard.socket.model.Message;
import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Date;
import java.util.Objects;

public class GameMessage extends Message {

    private GameMessageType gameMessageType;
    private WizardGame wizardGame;

    @Override
    public String toString() {
        return "GameMessage{" +
                "gameMessageType=" + gameMessageType +
                ", wizardGame=" + wizardGame +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GameMessage that = (GameMessage) o;
        return gameMessageType == that.gameMessageType &&
                Objects.equals(wizardGame, that.wizardGame);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gameMessageType, wizardGame);
    }

    public GameMessage(String sender){
        super(sender);
    }

    public GameMessage(String absender, Date messageSentAt, Date messageReceivedByServerAt, MessageContent content, String receiverChannel, Enum type, WizardGame wizardGame) {
        super(absender, messageSentAt, messageReceivedByServerAt, content, type);
        this.gameMessageType = (GameMessageType) type;
        this.wizardGame = wizardGame;
    }

    public GameMessage(String absender, Date messageSentAt, MessageContent content, String receiverChannel, Enum type, WizardGame wizardGame) {
        super(absender, messageSentAt, content, type);
        this.gameMessageType = (GameMessageType) type;
        this.wizardGame = wizardGame;
    }

    public GameMessageType getGameMessageType() {
        return gameMessageType;
    }

    public void setGameMessageType(GameMessageType gameMessageType) {
        this.gameMessageType = gameMessageType;
    }

    public WizardGame getWizardGame() {
        return wizardGame;
    }

    public void setWizardGame(WizardGame wizardGame) {
        this.wizardGame = wizardGame;
    }
}

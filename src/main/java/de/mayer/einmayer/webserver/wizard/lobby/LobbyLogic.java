package de.mayer.einmayer.webserver.wizard.lobby;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.dao.UserRepo;
import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.wizard.socket.Channels;
import de.mayer.einmayer.webserver.wizard.socket.model.Message;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.CreateGameContent;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.SimpleLobbyMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.LobbyMessageType;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.UserLeftLobbyContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

@Component
public class LobbyLogic {

    private static Logger log = LoggerFactory.getLogger(LobbyLogic.class);

    public SimpleLobbyMessage propagateNewGameMessage(WizardGame game, String username){
        User user = userRepo
                .findById(username)
                .orElseThrow(()->
                        new IllegalArgumentException("Nutzer konnte nicht gefunden werden!"));
        user.setPassword("secret");
        user.setRolle("secret");
        CreateGameContent content = new CreateGameContent(game, user);
        return new SimpleLobbyMessage(content, Message.SENDER_SERVER, LobbyMessageType.CREATE_GAME);
    }

    public void userLeft(String username){
        if (username == null) return;
        User user = userRepo.findById(username).orElse(null);
        if (user == null) {
            log.warn(String.format("Unbekannter Nutzer mit Namen %s wollte die Lobby verlassen!", username));
            return;
        }

        usersInLobby.removeUser(user);
        UserLeftLobbyContent content = new UserLeftLobbyContent(user);
        SimpleLobbyMessage lobbyCreateGameMessage = new SimpleLobbyMessage(content, Message.SENDER_SERVER, LobbyMessageType.LEAVE);
        messageOperations.convertAndSend(Channels.LOBBY + "/public", lobbyCreateGameMessage);

    }

    private SimpMessageSendingOperations messageOperations;

    @Autowired
    public void setMessageOperations(SimpMessageSendingOperations messageOperations) {
        this.messageOperations = messageOperations;
    }

    private UserRepo userRepo;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    private UsersInLobby usersInLobby;

    @Autowired
    public void setUsersInLobby(UsersInLobby usersInLobby) {
        this.usersInLobby = usersInLobby;
    }
}

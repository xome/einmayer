package de.mayer.einmayer.webserver.wizard.socket.model.game;

public enum GameMessageType {

    JOIN,
    GAME_IS_READY_TO_START,
    LEAVE,
    PLAY_CARD,
    CHAT,
    DROP,
    REPORT_ASSUMPTION

}

package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.dao.wizard.WizardKarte;
import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Objects;

public class PlayCardContent extends MessageContent {

    private WizardKarte wizardKarte;

    public PlayCardContent(WizardKarte wizardKarte) {
        this.wizardKarte = wizardKarte;
    }

    @Override
    public String toString() {
        return "PlayCardContent{" +
                "wizardKarte=" + wizardKarte +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayCardContent that = (PlayCardContent) o;
        return Objects.equals(wizardKarte, that.wizardKarte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wizardKarte);
    }

    public WizardKarte getWizardKarte() {
        return wizardKarte;
    }

    public void setWizardKarte(WizardKarte wizardKarte) {
        this.wizardKarte = wizardKarte;
    }
}

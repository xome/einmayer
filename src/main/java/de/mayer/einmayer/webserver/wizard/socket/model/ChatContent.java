package de.mayer.einmayer.webserver.wizard.socket.model;

import java.util.Objects;
import java.util.Set;

public class ChatContent extends MessageContent {

    private String message;
    private Set<String> recipients;

    public ChatContent(String message, Set<String> recipients) {
        this.message = message;
        this.recipients = recipients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatContent that = (ChatContent) o;
        return Objects.equals(message, that.message) &&
                Objects.equals(recipients, that.recipients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, recipients);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Set<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(Set<String> recipients) {
        this.recipients = recipients;
    }
}

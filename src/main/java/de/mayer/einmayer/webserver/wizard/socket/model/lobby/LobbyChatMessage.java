package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.wizard.socket.model.ChatContent;
import de.mayer.einmayer.webserver.wizard.socket.model.Message;

import java.util.Objects;

public class LobbyChatMessage extends Message {

    private ChatContent content;
    private LobbyMessageType msgType;

    public LobbyChatMessage(ChatContent content, String sender) {
        super(sender);
        this.content = content;
        this.msgType = LobbyMessageType.CHAT;
    }

    @Override
    public String toString() {
        return "LobbyChatMessage{" +
                "chatContent=" + content +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LobbyChatMessage that = (LobbyChatMessage) o;
        return Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), content);
    }

    public ChatContent getContent() {
        return content;
    }

    public void setContent(ChatContent content) {
        this.content = content;
    }

    public LobbyMessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(LobbyMessageType msgType) {
        this.msgType = msgType;
    }
}

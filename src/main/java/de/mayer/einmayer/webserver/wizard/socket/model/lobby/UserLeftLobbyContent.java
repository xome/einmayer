package de.mayer.einmayer.webserver.wizard.socket.model.lobby;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.wizard.socket.model.MessageContent;

import java.util.Objects;

public class UserLeftLobbyContent extends MessageContent {

    private User user;

    public UserLeftLobbyContent(User user) {
        this.user = user;
        this.user.setRolle("secret");
        this.user.setPassword("secret");
    }

    @Override
    public String toString() {
        return "UserLeftContent{" +
                "user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserLeftLobbyContent that = (UserLeftLobbyContent) o;
        return Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

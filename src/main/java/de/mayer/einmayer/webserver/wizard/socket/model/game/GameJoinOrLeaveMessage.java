package de.mayer.einmayer.webserver.wizard.socket.model.game;

import de.mayer.einmayer.webserver.wizard.socket.model.Message;

import java.util.Date;
import java.util.Objects;

public class GameJoinOrLeaveMessage extends Message {

    private GameMessageType msgType;
    private Integer gameId;
    private Integer seat;

    public GameJoinOrLeaveMessage(String sender, GameMessageType msgType, Integer gameId, Date messageSentAt) {
        super(sender);
        setMessageSentAt(messageSentAt);
        setGameId(gameId);
        this.msgType = msgType;
    }

    @Override
    public String toString() {
        return "GameJoinOrLeaveMessage{" +
                "msgType=" + msgType +
                ", gameId=" + gameId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GameJoinOrLeaveMessage that = (GameJoinOrLeaveMessage) o;
        return msgType == that.msgType &&
                Objects.equals(gameId, that.gameId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), msgType, gameId);
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public GameMessageType getMsgType() {
        return msgType;
    }

    public void setMsgType(GameMessageType msgType) {
        this.msgType = msgType;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }
}

package de.mayer.einmayer.webserver.wizard.socket.model;

import java.util.Date;
import java.util.Objects;

public abstract class Message {

    public static final String SENDER_SERVER = "Server";

    private String sender;
    private Date messageSentAt;
    private Date messageReceivedByServerAt;
    private Enum type;

    public Message(String sender){
        setMessageReceivedByServerAt(new Date());
        setSender(sender);
        if (sender.equals(Message.SENDER_SERVER)){
            setMessageSentAt(getMessageReceivedByServerAt());
        }
    }

    public Message(String sender, Date messageSentAt, Date messageReceivedByServerAt, MessageContent content, Enum type) {
        setMessageReceivedByServerAt(new Date());
        if (sender.equals(Message.SENDER_SERVER)){
            setMessageSentAt(getMessageReceivedByServerAt());
        }
        this.sender = sender;
        this.messageSentAt = messageSentAt;
        this.messageReceivedByServerAt = messageReceivedByServerAt;
        this.type = type;
    }


    public Message(String sender, Date messageSentAt, MessageContent content, Enum type) {
        setMessageReceivedByServerAt(new Date());
        if (sender.equals(Message.SENDER_SERVER)){
            setMessageSentAt(getMessageReceivedByServerAt());
        }
        this.sender = sender;
        this.messageSentAt = messageSentAt;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender='" + sender + '\'' +
                ", messageSentAt=" + messageSentAt +
                ", messageReceivedByServerAt=" + messageReceivedByServerAt +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(sender, message.sender) &&
                Objects.equals(messageSentAt, message.messageSentAt) &&
                Objects.equals(messageReceivedByServerAt, message.messageReceivedByServerAt) &&
                Objects.equals(type, message.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, messageSentAt, messageReceivedByServerAt,
                 type);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getMessageSentAt() {
        return messageSentAt;
    }

    public void setMessageSentAt(Date messageSentAt) {
        this.messageSentAt = messageSentAt;
    }

    public Date getMessageReceivedByServerAt() {
        return messageReceivedByServerAt;
    }

    public void setMessageReceivedByServerAt(Date messageReceivedByServerAt) {
        this.messageReceivedByServerAt = messageReceivedByServerAt;
    }


}

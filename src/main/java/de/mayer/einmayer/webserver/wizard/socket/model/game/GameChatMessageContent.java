package de.mayer.einmayer.webserver.wizard.socket.model.game;

import java.util.Objects;

public class GameChatMessageContent {

    private String message;
    private Integer idGame;


    public GameChatMessageContent(String message, Integer idGame) {
        this.message = message;
        this.idGame = idGame;
    }

    @Override
    public String toString() {
        return "GameChatMessageContent{" +
                "message='" + message + '\'' +
                ", idGame=" + idGame +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameChatMessageContent that = (GameChatMessageContent) o;
        return Objects.equals(message, that.message) &&
                Objects.equals(idGame, that.idGame);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, idGame);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getIdGame() {
        return idGame;
    }

    public void setIdGame(Integer idGame) {
        this.idGame = idGame;
    }
}

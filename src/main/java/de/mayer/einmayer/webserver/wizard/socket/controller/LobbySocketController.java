package de.mayer.einmayer.webserver.wizard.socket.controller;

import de.mayer.einmayer.webserver.dao.UserRepo;
import de.mayer.einmayer.webserver.dao.wizard.WizardGame;
import de.mayer.einmayer.webserver.dao.wizard.WizardGameParticipantRepo;
import de.mayer.einmayer.webserver.dao.wizard.WizardGameRepo;
import de.mayer.einmayer.webserver.wizard.game.GameLogic;
import de.mayer.einmayer.webserver.wizard.lobby.LobbyLogic;
import de.mayer.einmayer.webserver.wizard.socket.Channels;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.LobbyChatMessage;
import de.mayer.einmayer.webserver.wizard.socket.model.SessionAttributes;
import de.mayer.einmayer.webserver.wizard.socket.model.lobby.SimpleLobbyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

@Controller
public class LobbySocketController {

    @MessageMapping("game.create")
    @SendTo(Channels.LOBBY + "/public")
    public SimpleLobbyMessage createGame(@Payload SimpleLobbyMessage lobbyCreateGameMessage,
                                         SimpMessageHeaderAccessor headerAccessor) {
        WizardGame game = gameLogic.newGame();
        SessionAttributes sessionAttributes = new SessionAttributes(headerAccessor.getSessionAttributes());
        if (sessionAttributes.getName() == null) {
            sessionAttributes.setName(lobbyCreateGameMessage.getSender());
        }
        sessionAttributes.setGameId(game.getId());
        sessionAttributes.updateWebsocketSessionAttributes(headerAccessor);
        lobbyLogic.userLeft(sessionAttributes.getName());
        return lobbyLogic.propagateNewGameMessage(game, lobbyCreateGameMessage.getSender());
    }


    @MessageMapping("lobby.chat")
    @SendTo(Channels.LOBBY + "/public")
    public LobbyChatMessage chatMessage(@Payload LobbyChatMessage msgIn,
                                              SimpMessageHeaderAccessor headerAccessor){
        msgIn.setSender(SecurityContextHolder.getContext().getAuthentication().getName());
        return msgIn;
    }

    private GameLogic gameLogic;

    @Autowired
    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    private LobbyLogic lobbyLogic;

    @Autowired
    public void setLobbyLogic(LobbyLogic lobbyLogic) {
        this.lobbyLogic = lobbyLogic;
    }

    private WizardGameRepo wizardGameRepo;

    @Autowired
    public void setWizardGameRepo(WizardGameRepo wizardGameRepo) {
        this.wizardGameRepo = wizardGameRepo;
    }

    private WizardGameParticipantRepo wizardGameParticipantRepo;

    @Autowired
    public void setWizardGameParticipantRepo(WizardGameParticipantRepo wizardGameParticipantRepo) {
        this.wizardGameParticipantRepo = wizardGameParticipantRepo;
    }

    private UserRepo userRepo;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
}

package de.mayer.einmayer.webserver.dao.wizard;

import java.io.Serializable;
import java.util.Objects;

public class WizardGameParticipantId implements Serializable {

    private Integer idGame;
    private String nameUser;

    public WizardGameParticipantId(){

    }

    public WizardGameParticipantId(Integer idGame, String nameUser) {
        this.idGame = idGame;
        this.nameUser = nameUser;
    }

    @Override
    public String toString() {
        return "WizardGameParticipantId{" +
                "idGame=" + idGame +
                ", nameUser='" + nameUser + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WizardGameParticipantId that = (WizardGameParticipantId) o;
        return Objects.equals(idGame, that.idGame) &&
                Objects.equals(nameUser, that.nameUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGame, nameUser);
    }

    public Integer getIdGame() {
        return idGame;
    }

    public void setIdGame(Integer idGame) {
        this.idGame = idGame;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }
}

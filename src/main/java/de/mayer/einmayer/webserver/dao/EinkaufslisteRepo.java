package de.mayer.einmayer.webserver.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface EinkaufslisteRepo extends JpaRepository<Einkaufsliste, Integer> {
    Optional<Einkaufsliste> findByName(String name);
    @Transactional
    void deleteByName(String test);

    boolean existsByName(String name);

    List<Einkaufsliste> findByStatus(String status);

    @Transactional
    void deleteByStatus(String statusGeloescht);
}

package de.mayer.einmayer.webserver.dao;

import java.io.Serializable;
import java.util.Objects;

public class EinkaufslisteItemId implements Serializable {

    public EinkaufslisteItemId() {
    }

    private Integer einkaufslisteId;
    private String item;

    public EinkaufslisteItemId(Integer einkaufsliste, String item) {
        this.einkaufslisteId = einkaufsliste;
        this.item = item;
    }

    public Integer getEinkaufslisteId() {
        return einkaufslisteId;
    }

    public void setEinkaufslisteId(Integer einkaufslisteId) {
        this.einkaufslisteId = einkaufslisteId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "EinkaufslisteItemId{" +
                "einkaufslisteId=" + einkaufslisteId +
                ", item='" + item + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EinkaufslisteItemId that = (EinkaufslisteItemId) o;
        return Objects.equals(einkaufslisteId, that.einkaufslisteId) &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {

        return Objects.hash(einkaufslisteId, item);
    }
}

package de.mayer.einmayer.webserver.dao.wizard;

public enum WizardGameStat {
    CLOSED,
    OPEN
}

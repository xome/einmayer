package de.mayer.einmayer.webserver.dao.wizard;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class WizardFarbe {
    @Id
    private String id;
    private Integer farbeR;
    private Integer farbeG;
    private Integer farbeB;

    public WizardFarbe() {
    }

    @Override
    public String toString() {
        return "WizardFarben{" +
                "id='" + id + '\'' +
                ", farbeR=" + farbeR +
                ", farbeG=" + farbeG +
                ", farbeB=" + farbeB +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WizardFarbe that = (WizardFarbe) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(farbeR, that.farbeR) &&
                Objects.equals(farbeG, that.farbeG) &&
                Objects.equals(farbeB, that.farbeB);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, farbeR, farbeG, farbeB);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getFarbeR() {
        return farbeR;
    }

    public void setFarbeR(Integer farbeR) {
        this.farbeR = farbeR;
    }

    public Integer getFarbeG() {
        return farbeG;
    }

    public void setFarbeG(Integer farbeG) {
        this.farbeG = farbeG;
    }

    public Integer getFarbeB() {
        return farbeB;
    }

    public void setFarbeB(Integer farbeB) {
        this.farbeB = farbeB;
    }
}

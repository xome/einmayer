package de.mayer.einmayer.webserver.dao.wizard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WizardZahlRepo extends JpaRepository<WizardZahl, String> {
}

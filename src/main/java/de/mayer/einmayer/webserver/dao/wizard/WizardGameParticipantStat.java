package de.mayer.einmayer.webserver.dao.wizard;

public enum WizardGameParticipantStat {
    IN_GAME,
    DROPPED,
    LEFT
}

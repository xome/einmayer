package de.mayer.einmayer.webserver.dao.wizard;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@IdClass(WizardGameParticipantId.class)
public class WizardGameParticipant {

    @Id
    private Integer idGame;
    @Id
    private String nameUser;

    private Integer seat;
    private String stat;

    @Override
    public String toString() {
        return "WizardGameParticipant{" +
                "idGame=" + idGame +
                ", nameUser='" + nameUser + '\'' +
                ", seat=" + seat +
                ", stat='" + stat + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WizardGameParticipant that = (WizardGameParticipant) o;
        return Objects.equals(idGame, that.idGame) &&
                Objects.equals(nameUser, that.nameUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGame, nameUser);
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public Integer getIdGame() {
        return idGame;
    }

    public void setIdGame(Integer idGame) {
        this.idGame = idGame;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }
}

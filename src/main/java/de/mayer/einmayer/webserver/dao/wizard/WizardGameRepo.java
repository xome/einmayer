package de.mayer.einmayer.webserver.dao.wizard;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WizardGameRepo extends JpaRepository<WizardGame, Integer> {
    List<WizardGame> findByRound(Integer round);

    List<WizardGame> findByRoundAndStat(int id, String stat);

}

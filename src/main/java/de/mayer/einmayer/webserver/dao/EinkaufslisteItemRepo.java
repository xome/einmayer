package de.mayer.einmayer.webserver.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface EinkaufslisteItemRepo extends JpaRepository<EinkaufslisteItems, EinkaufslisteItemId> {


    boolean existsByEinkaufslisteIdAndItemAndStatus(Integer id, String item, String s);

    List<EinkaufslisteItems> findByEinkaufslisteId(Integer id);

    List<EinkaufslisteItems> findByEinkaufslisteIdAndStatusIn(Integer id, List<String> status);

    @Transactional
    void deleteByStatus(String statusGeloescht);
}

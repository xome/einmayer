package de.mayer.einmayer.webserver.dao.wizard;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class WizardKarte {

    @Id
    private String id;
    private Integer idFarbe;
    private String idZahl;

    public WizardKarte() {
    }

    @Override
    public String toString() {
        return "WizardKarten{" +
                "id='" + id + '\'' +
                ", idFarbe=" + idFarbe +
                ", idZahl='" + idZahl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WizardKarte that = (WizardKarte) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(idFarbe, that.idFarbe) &&
                Objects.equals(idZahl, that.idZahl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idFarbe, idZahl);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdFarbe() {
        return idFarbe;
    }

    public void setIdFarbe(Integer idFarbe) {
        this.idFarbe = idFarbe;
    }

    public String getIdZahl() {
        return idZahl;
    }

    public void setIdZahl(String idZahl) {
        this.idZahl = idZahl;
    }
}

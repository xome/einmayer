package de.mayer.einmayer.webserver.dao.wizard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface WizardGameParticipantRepo extends JpaRepository<WizardGameParticipant, WizardGameParticipantId> {
    List<WizardGameParticipant> findByIdGame(Integer idGame);
    WizardGameParticipant findByNameUserAndStat(String user, String stat);
    Long countByIdGame(Integer idGame);
    @Query("select max(u.seat) from WizardGameParticipant u where u.idGame = ?1")
    Long maxByIdGame(Integer idGame);
    Long countByIdGameAndStat(Integer idGame, String stat);
    List<WizardGameParticipant> findAllByIdGame(Integer gameId);
    List<WizardGameParticipant> findByIdGameAndStatIn(Integer id, List<String> status);


}

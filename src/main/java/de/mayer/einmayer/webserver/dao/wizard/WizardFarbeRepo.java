package de.mayer.einmayer.webserver.dao.wizard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WizardFarbeRepo extends JpaRepository<WizardFarbe, String> {
}

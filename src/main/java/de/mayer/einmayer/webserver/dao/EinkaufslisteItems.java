package de.mayer.einmayer.webserver.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@IdClass(EinkaufslisteItemId.class)
public class EinkaufslisteItems {

    public static final String STATUS_OFFEN = "10";
    public static final String STATUS_CHECKED = "50";
    public static final String STATUS_GELOESCHT = "80";


    public EinkaufslisteItems() {
    }

    public EinkaufslisteItems(Integer einkaufslisteId, String item) {
        this.einkaufslisteId = einkaufslisteId;
        this.item = item;
    }

    @Id
    private Integer einkaufslisteId;
    @Id
    private String item;

    private String status;
    private Timestamp timeNeu;
    private Timestamp timeAen;


    @Override
    public String toString() {
        return "EinkaufslisteItems{" +
                ", einkaufslisteId=" + einkaufslisteId +
                ", item='" + item + '\'' +
                ", status='" + status + '\'' +
                ", timeNeu=" + timeNeu +
                ", timeAen=" + timeAen +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EinkaufslisteItems that = (EinkaufslisteItems) o;
               return Objects.equals(einkaufslisteId, that.einkaufslisteId) &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {

        return Objects.hash(einkaufslisteId, item);
    }

    public Integer getEinkaufslisteId() {
        return einkaufslisteId;
    }

    public void setEinkaufslisteId(Integer einkaufslisteId) {
        this.einkaufslisteId = einkaufslisteId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getTimeNeu() {
        return timeNeu;
    }

    public void setTimeNeu(Timestamp timeNeu) {
        this.timeNeu = timeNeu;
    }

    public Timestamp getTimeAen() {
        return timeAen;
    }

    public void setTimeAen(Timestamp timeAen) {
        this.timeAen = timeAen;
    }
}

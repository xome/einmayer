package de.mayer.einmayer.webserver.dao;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

@Entity
public class Einkaufsliste {

    public static final String STATUS_OFFEN = "10";
    public static final String STATUS_GELOESCHT = "80";
    public static final String STATUS_OFFLINE_ANGELEGT = "00";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String status;
    private Timestamp timeNeu;
    private Timestamp timeAen;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getTimeNeu() {
        return timeNeu;
    }

    public void setTimeNeu(Timestamp timeNeu) {
        this.timeNeu = timeNeu;
    }

    public Timestamp getTimeAen() {
        return timeAen;
    }

    public void setTimeAen(Timestamp timeAen) {
        this.timeAen = timeAen;
    }


    @Override
    public String toString() {
        return "Einkaufsliste{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", timeNeu=" + timeNeu +
                ", timeAen=" + timeAen +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Einkaufsliste that = (Einkaufsliste) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(timeNeu, that.timeNeu) &&
                Objects.equals(timeAen, that.timeAen)     ;}

    @Override
    public int hashCode() {

        return Objects.hash(id, name, status, timeNeu, timeAen);
    }
}

package de.mayer.einmayer.webserver.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
public class ReminderDate {

    public static final String STATUS_OFFEN ="10";
    public static final String STATUS_ERLEDIGT = "50";
    public static final String STATUS_NICHT_ERLEDIGT = "60";
    public static final String STATUS_GELOESCHT = "80";



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Timestamp datum;
    private String titel;
    private String beschreibung;
    private String status;
    private String zielAnwender;
    private String anwenderNeu;
    private Timestamp timeAen;
    private Timestamp timeNeu;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReminderDate that = (ReminderDate) o;
        return Objects.equals(id, that.id);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnwenderNeu() {
        return anwenderNeu;
    }

    public void setAnwenderNeu(String anwenderNeu) {
        this.anwenderNeu = anwenderNeu;
    }

    @Override
    public String toString() {
        return "ReminderDate{" +
                "id=" + id +
                ", datum=" + datum +
                ", titel='" + titel + '\'' +
                ", beschreibung='" + beschreibung + '\'' +
                ", status='" + status + '\'' +
                ", zielAnwender='" + zielAnwender + '\'' +
                ", anwenderNeu='" + anwenderNeu + '\'' +
                ", timeAen=" + timeAen +
                ", timeNeu=" + timeNeu +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZielAnwender() {
        return zielAnwender;
    }

    public void setZielAnwender(String zielAnwender) {
        this.zielAnwender = zielAnwender;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Timestamp getTimeAen() {
        return timeAen;
    }

    public void setTimeAen(Timestamp timeAen) {
        this.timeAen = timeAen;
    }

    public Timestamp getTimeNeu() {
        return timeNeu;
    }

    public void setTimeNeu(Timestamp timeNeu) {
        this.timeNeu = timeNeu;
    }

    public Timestamp getDatum() {
        return datum;
    }

    public void setDatum(Timestamp datum) {
        this.datum = datum;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
}

package de.mayer.einmayer.webserver.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ReminderDateRepo extends JpaRepository<ReminderDate, Long> {
    List<ReminderDate> findByZielAnwenderInAndStatusInOrAnwenderNeuAndStatusIn(List<String> beide, List<String> statiOffen, String userLogon, List<String> statiOffen1);

    @Query("select t " +
            "from ReminderDate t " +
            "where (zielAnwender = :userLogon or anwenderNeu = :userLogon)" +
            "  and status in :statiOffen" +
            "  and datum between :datumStart and :datumEnde")
    List<ReminderDate> findFuerHeute(@Param("userLogon") String userLogon, @Param("statiOffen") List<String> statiOffen,
                                     @Param("datumStart") Date datumStart, @Param("datumEnde") Date datumEnde);

}

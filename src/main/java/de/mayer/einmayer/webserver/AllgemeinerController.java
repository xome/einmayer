package de.mayer.einmayer.webserver;

import de.mayer.einmayer.webserver.dao.User;
import de.mayer.einmayer.webserver.dao.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AllgemeinerController {


    @RequestMapping(method = RequestMethod.GET, value = "/get_user")
    public @ResponseBody User getUser(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepo.findById(username).orElse(null);
    }

    private UserRepo userRepo;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
}

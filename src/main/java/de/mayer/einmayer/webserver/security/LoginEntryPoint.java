package de.mayer.einmayer.webserver.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginEntryPoint implements AuthenticationEntryPoint {

    private Logger log = LoggerFactory.getLogger(LoginEntryPoint.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        log.warn("Unauthorized Request to " + httpServletRequest.getRequestURI() + " from " + httpServletRequest.getLocalAddr());
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "So nicht!");
    }

}

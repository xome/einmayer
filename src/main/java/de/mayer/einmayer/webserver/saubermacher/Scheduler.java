package de.mayer.einmayer.webserver.saubermacher;

import de.mayer.einmayer.webserver.dao.Einkaufsliste;
import de.mayer.einmayer.webserver.dao.EinkaufslisteItems;
import de.mayer.einmayer.webserver.dao.EinkaufslisteItemRepo;
import de.mayer.einmayer.webserver.dao.EinkaufslisteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class Scheduler {


    @Scheduled(cron="0 0 * * * ?")
    public void deleteRemovedListsAndItems(){
        einkaufslisteRepo.deleteByStatus(Einkaufsliste.STATUS_GELOESCHT);
        einkaufslisteItemRepo.deleteByStatus(EinkaufslisteItems.STATUS_GELOESCHT);
    }


    private EinkaufslisteRepo einkaufslisteRepo;

    @Autowired
    public void setEinkaufslisteRepo(EinkaufslisteRepo einkaufslisteRepo) {
        this.einkaufslisteRepo = einkaufslisteRepo;
    }

    private EinkaufslisteItemRepo einkaufslisteItemRepo;

    @Autowired
    public void setEinkaufslisteItemRepo(EinkaufslisteItemRepo einkaufslisteItemRepo) {
        this.einkaufslisteItemRepo = einkaufslisteItemRepo;
    }
}

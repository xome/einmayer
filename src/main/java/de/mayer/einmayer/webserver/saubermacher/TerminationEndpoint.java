package de.mayer.einmayer.webserver.saubermacher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TerminationEndpoint implements ApplicationContextAware {

    private ApplicationContext ctx;

    private final Logger log = LoggerFactory.getLogger(TerminationEndpoint.class);

    @PostMapping("/system/shutdown")
    public void shutdown(){
        log.warn("Killing myself.");
        ((ConfigurableApplicationContext) ctx).close();
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}

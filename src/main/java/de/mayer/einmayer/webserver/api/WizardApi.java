package de.mayer.einmayer.webserver.api;

import ch.qos.logback.core.boolex.EvaluationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WizardApi {

    @RequestMapping(method = RequestMethod.GET, value = "wizard/default_password")
    public String getDefaultPassword() {
        return "\"\"";
    }


}

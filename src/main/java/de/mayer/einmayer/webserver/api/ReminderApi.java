package de.mayer.einmayer.webserver.api;

import de.mayer.einmayer.webserver.dao.ReminderDate;
import de.mayer.einmayer.webserver.dao.ReminderDateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class ReminderApi {


    @RequestMapping(value = "/api/reminder/edit_or_add_date", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> editOrAddDate(@RequestBody ReminderDate date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. 'um' HH 'Uhr' mm");
        if (!repo.existsById(date.getId())){
            date.setTimeNeu(new Timestamp(System.currentTimeMillis()));
            date.setAnwenderNeu(
                    ((User)SecurityContextHolder
                            .getContext()
                            .getAuthentication()
                            .getPrincipal())
                            .getUsername()
            );
        }
        String resp;
        if (date.getDatum().compareTo(new Timestamp(System.currentTimeMillis())) <= 0){
            date.setStatus(ReminderDate.STATUS_NICHT_ERLEDIGT);
            resp = "Der Termin liegt in der Vergangenheit! Trotzdem gespeichert.";
        } else {
            date.setStatus(ReminderDate.STATUS_OFFEN);
            resp = String.format("Du wirst am %s an %s erinnert.", sdf.format(date.getDatum()), date.getTitel());
        }
        if (date.getZielAnwender() == null)
            date.setZielAnwender("beide");

        date.setTimeAen(new Timestamp(System.currentTimeMillis()));
        date = repo.save(date);
        HashMap<String, Object> ret = new HashMap<>();
        ret.put("response", resp);
        ret.put("date", date);
        return ret;
    }

    @RequestMapping(value = "/api/reminder/get_date_by_id", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ReminderDate getDateById(@RequestBody ReminderDate date) {
        return repo.findById(date.getId()).orElse(null);
    }

    @RequestMapping(value="/api/reminder/delete_date", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> deleteDate(@RequestBody ReminderDate date){
        date.setStatus(ReminderDate.STATUS_GELOESCHT);
        date.setTimeAen(new Timestamp(System.currentTimeMillis()));
        repo.save(date);
        return Collections.singletonMap("response", "Erinnderung wurde erfolgreich gelöscht.");
    }

    @RequestMapping(value="/api/reminder/get_all_dates", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<ReminderDate> getAllDates(){
        String userLogon = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        List<String> statiOffen = Arrays.asList(ReminderDate.STATUS_OFFEN, ReminderDate.STATUS_ERLEDIGT, ReminderDate.STATUS_NICHT_ERLEDIGT);
        return repo.findByZielAnwenderInAndStatusInOrAnwenderNeuAndStatusIn(Arrays.asList("beide", userLogon), statiOffen,
                userLogon, statiOffen
                );
    }

    @RequestMapping(value="/api/reminder/get_fuer_heute", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<ReminderDate> getFuerHeute(){
        String userLogon = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        List<String> statiOffen = Arrays.asList(ReminderDate.STATUS_OFFEN, ReminderDate.STATUS_ERLEDIGT, ReminderDate.STATUS_NICHT_ERLEDIGT);
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date start = cal.getTime();
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return repo.findFuerHeute(userLogon, statiOffen, start, cal.getTime());
    }

    @RequestMapping(value = "/api/reminder/get_fuer_push", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<ReminderDate> getFuerPush(){
        String userLogon = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        List<String> statiOffen = Arrays.asList(ReminderDate.STATUS_OFFEN, ReminderDate.STATUS_ERLEDIGT, ReminderDate.STATUS_NICHT_ERLEDIGT);
        Date now = new Date();
        Date then = new Date(now.getTime() + 15*60*1000); // in 15 Minuten
        return repo.findFuerHeute(userLogon, statiOffen, now, then);
    }

    private ReminderDateRepo repo;

    @Autowired
    public void setRepo(ReminderDateRepo repo) {
        this.repo = repo;
    }
}

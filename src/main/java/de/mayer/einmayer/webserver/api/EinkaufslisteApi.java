package de.mayer.einmayer.webserver.api;

import de.mayer.einmayer.webserver.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
public class EinkaufslisteApi {

    @RequestMapping(value="/api/einkaufsliste/get_liste_by_name", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody Einkaufsliste getListeByName(@RequestBody String name){
        return einkaufslisteRepo.findByName(name).orElse(null);
    }
    @RequestMapping(value="/api/einkaufsliste/get_item", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    EinkaufslisteItems getListeByName(@RequestBody EinkaufslisteItems restItem){
        return einkaufslisteItemRepo.findById(new EinkaufslisteItemId(restItem.getEinkaufslisteId(), restItem.getItem())).orElse(null);
    }



    @RequestMapping(value="/api/einkaufsliste/add_item", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> addItem(@RequestBody EinkaufslisteItems restItem) {
        Einkaufsliste einkaufsliste = einkaufslisteRepo
                .findById(restItem.getEinkaufslisteId())
                .orElseThrow(() ->
                        new RuntimeException(String.format(
                                "Einkaufsliste %s konnte nicht gefunden werden!", restItem.getEinkaufslisteId())));

        if (einkaufslisteItemRepo
                .existsByEinkaufslisteIdAndItemAndStatus(einkaufsliste.getId(), restItem.getItem(), "10"))
            throw new RuntimeException(String.format("%s ist bereits auf der Liste.", restItem.getItem()));

        restItem.setStatus(EinkaufslisteItems.STATUS_OFFEN);
        restItem.setTimeAen(new Timestamp(System.currentTimeMillis()));
        restItem.setTimeNeu(new Timestamp(System.currentTimeMillis()));
        einkaufslisteItemRepo.save(restItem);

        return Collections.singletonMap("response", String.format("%s wurde hinzugefügt.", restItem.getItem()));
    }

    @RequestMapping(value="/api/einkaufsliste/add_liste", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> addListe(@RequestBody Einkaufsliste liste) {
        if (liste.getName() == null || liste.getName().trim().isEmpty())
            throw new RuntimeException("Die Liste muss einen Namen bekommen.");

        liste = einkaufslisteRepo
                .findByName(liste.getName())
                .orElse(liste);

        if (Einkaufsliste.STATUS_OFFEN.equals(liste.getStatus()))
            throw new RuntimeException(String.format("Eine Liste mit dem Namen %s existiert bereits.", liste.getName()));

        String ret = null;
        if (Einkaufsliste.STATUS_GELOESCHT.equals(liste.getStatus())){
            ret = "%s ist wieder verfügbar";
        } else
            ret = "Liste %s wurde angelegt.";

        liste.setTimeNeu(new Timestamp(System.currentTimeMillis()));
        liste.setTimeAen(new Timestamp(System.currentTimeMillis()));
        liste.setStatus(Einkaufsliste.STATUS_OFFEN);
        einkaufslisteRepo.save(liste);
        return Collections.singletonMap("response", String.format(ret, liste.getName()));
    }


    @RequestMapping(value="/api/einkaufsliste/remove_item", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> removeItem(@RequestBody EinkaufslisteItems item) {
        if (!einkaufslisteItemRepo.existsByEinkaufslisteIdAndItemAndStatus(item.getEinkaufslisteId(), item.getItem(), "10"))
            return Collections.singletonMap("response", String.format("%s ist gar nicht auf der Liste enthalten.", item.getItem()));

        item.setStatus(EinkaufslisteItems.STATUS_GELOESCHT);
        item.setTimeAen(new Timestamp(System.currentTimeMillis()));
        einkaufslisteItemRepo.save(item);
        return Collections.singletonMap("response", String.format("%s wurde von der Liste entfernt.", item.getItem()));
    }


    @RequestMapping(value="/api/einkaufsliste/check_item",method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> checkItem(@RequestBody final EinkaufslisteItems restItem) {
        EinkaufslisteItems item = einkaufslisteItemRepo
                .findById(new EinkaufslisteItemId(restItem.getEinkaufslisteId(), restItem.getItem()))
                .orElseThrow(() ->
                        new RuntimeException(String.format("%s ist nicht auf der Liste.", restItem.getItem())));

        item.setTimeAen(new Timestamp(System.currentTimeMillis()));
        String ret = null;
        if (item.getStatus().equals(EinkaufslisteItems.STATUS_CHECKED)){
            item.setStatus(EinkaufslisteItems.STATUS_OFFEN);
            ret = "%s ist jetzt wieder offen";
        } else {
            item.setStatus(EinkaufslisteItems.STATUS_CHECKED);
            ret = "%s wurde abgehakt.";
        }
        einkaufslisteItemRepo.save(item);

        return Collections.singletonMap("response", String.format(ret, item.getItem()));
    }

    @RequestMapping(value="/api/einkaufsliste/get_all_listen", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<Einkaufsliste> getAllListen() {
        return einkaufslisteRepo.findByStatus(Einkaufsliste.STATUS_OFFEN);
    }

    @RequestMapping(value="/api/einkaufsliste/get_posis_by_liste", method=RequestMethod.POST)
    public @ResponseBody List<EinkaufslisteItems> getPosisByListe(@RequestBody Einkaufsliste liste) {
        return einkaufslisteItemRepo.findByEinkaufslisteIdAndStatusIn(liste.getId(),
                Arrays.asList(EinkaufslisteItems.STATUS_OFFEN, EinkaufslisteItems.STATUS_CHECKED));
    }

    @RequestMapping(value="/api/einkaufsliste/remove_liste", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> removeListe(@RequestBody final Einkaufsliste restListe) {
        Einkaufsliste liste = einkaufslisteRepo
                .findById(restListe.getId())
                .orElseThrow(() -> new RuntimeException("Liste konnte nicht gefunden werden!"));

        liste.setStatus(Einkaufsliste.STATUS_GELOESCHT);
        liste.setTimeAen(new Timestamp(System.currentTimeMillis()));
        einkaufslisteRepo.save(liste);
        return Collections.singletonMap("response", String.format("Liste %s wurde gelöscht.", liste.getName()));
    }


    @RequestMapping(value="/api/einkaufsliste/clear_liste", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    Map<String, String> clearListe(@RequestBody Einkaufsliste liste){
        liste = einkaufslisteRepo
                .findById(liste.getId())
                .orElseThrow(() -> new RuntimeException("Liste nicht gefunden."));

        List<EinkaufslisteItems> items = einkaufslisteItemRepo.findByEinkaufslisteId(liste.getId());
        items.forEach(item -> item.setStatus(EinkaufslisteItems.STATUS_GELOESCHT));
        einkaufslisteItemRepo.saveAll(items);
        return Collections.singletonMap("response", String.format("Liste %s wurde erfolgreich geleert.", liste.getName()));

    }

    @RequestMapping(value="/api/einkaufsliste/update_liste", method=RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> updateListe(@RequestBody Einkaufsliste liste){
        einkaufslisteRepo.save(liste);
        return Collections.singletonMap("response", "Liste aktualisiert.");
    }

    @RequestMapping(value="/api/einkaufsliste/get_letzte_liste", method= RequestMethod.POST, produces = "application/json")
    public @ResponseBody Einkaufsliste getLetzteListe(){
        List<Einkaufsliste> list = einkaufslisteRepo
                .findByStatus(Einkaufsliste.STATUS_OFFEN);
        list.sort(Comparator.comparing(Einkaufsliste::getTimeAen).reversed());
        return list.get(0);
    }

    private EinkaufslisteRepo einkaufslisteRepo;

    private EinkaufslisteItemRepo einkaufslisteItemRepo;

    @Autowired
    public void setEinkaufslisteRepo(EinkaufslisteRepo einkaufslisteRepo) {
        this.einkaufslisteRepo = einkaufslisteRepo;
    }

    @Autowired
    public void setEinkaufslisteItemRepo(EinkaufslisteItemRepo einkaufslisteItemRepo) {
        this.einkaufslisteItemRepo = einkaufslisteItemRepo;
    }
}

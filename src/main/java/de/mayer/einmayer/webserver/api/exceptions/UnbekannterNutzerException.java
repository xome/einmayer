package de.mayer.einmayer.webserver.api.exceptions;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static java.lang.String.format;

public class UnbekannterNutzerException extends UsernameNotFoundException {
    public UnbekannterNutzerException(String name) {
        super(format("Der User %s ist nicht bekannt oder Passwort ist falsch!", name));
    }
}
